#include <boost/asio.hpp>
#include <boost/system/system_error.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/bind.hpp>

#include "../include/communications.h"

using namespace sefu;
using namespace sefu::comm;




int main( int _argc, char** _argv ) {
    
	
	boost::shared_ptr<udpTransmitter> t = newTransmitter<udpTransmitter>("127.0.0.1",9991);

	
	
	long time = 100000;
	while( true ) {
		std::stringstream ss;
		
		float x = 3*cos((double)time/200.0);
		float y = 3*sin((double)time/200.0);
		
		
		//"%s%d,%d,%f,%f,%f,%f,%f,%f,%f;"
		ss << time << ";ArduCopter_02 100,1," << x << "," << y << ",0.0,0.0,0.0,0.0;\0";
		
		t->sendString(ss.str());
		
		//std::cout << ss.str() << std::endl;
		
		time++;
		usleep(10000);
	}
	
	
	
	
    return 0;
}
