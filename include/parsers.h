/** \file parsers.h
 *  \brief Convenience header for processors
 *  \author Niklas Bergström
 *
 *  \details A \c processor is needed by the \c listener in order to 
 *  process the incoming sensor data. Basically one listener should
 *  be provided per sensor type. It is the task of the processor to
 *  fill a \c SensorStruct with relevant values. Whenever a new processor
 *  subclass is implemented, the function getProcessor should be altered
 *  to include generation of this processor given a specific type
 */
#ifndef _PROCESSORS_H_
#define _PROCESSORS_H_

#include "parsers/ptam_processor.h"
#include "parsers/vicon_processor.h"
#include "parsers/odo_processor.h"
#include "parsers/imu_processor.h"

#include "sf_exception.h"

/** \defgroup Processors Sensor Fusion Processors
 *  This group contains classes related to processors, i.e. classes
 *  meant for parsing and handling incoming sensor data
 */


/** namespace sefu */
namespace sefu {
    /** namespace sefu::proc */
    namespace pars {
        
        /** \fn inline processor* getProcessor( const std::string& _type, const std::string& _vehicleName )
         *  \brief Function for getting new instances of processor subclasses
         *  \ingroup Processors
         *
         *  \details The function takes a type (i.e. sensor type) and a vehicle name and returns
         *  the processor corresponding to the type.
         *
         *  \param _type String holding the sensor type
         *  \param _vehicleName String holding the name of the vehicle
         *  \return An initialized processor
         *
         *  \throw Throws an sfException if it failed to get the processor
         */
        inline processor* getProcessor( const std::string& _type, const std::string& _vehicleName ) {
            processor* p = NULL;
            
            if( !_type.compare("vicon") ) {
                p = new viconProcessor(_vehicleName);
            }
            else if( !_type.compare("ptam") ) {
                p = new ptamProcessor(_vehicleName);
            }
            else if( !_type.compare("odo") ) {
                p = new odoProcessor(_vehicleName);
            }
			else if( !_type.compare("imu") ) {
				p = new imuProcessor(_vehicleName);
			}
			
            if (!p) {
                throw sfException("Failed getting processor for type",_type);
            }
            
            return p;
        }
        
    } // namespace pars
} // namespace sefu

#endif // _PROCESSORS_H_
