#ifndef _COORDINATETRANSFORM_H_
#define _COORDINATETRANSFORM_H_

#include <iostream>
#include <vector>

#include <cmath>

namespace sefu {
	
	struct Matrix3x3;
	
	struct Vector3 {
		double v[3];
		
		Vector3();
		
		Vector3(double _x, double _y, double _z);
		
		double operator[](int _idx) const;
		
		double& operator[](int _idx);
		
		Vector3 operator*(double _a) const;
		
		Vector3 operator-() const;
		
		Vector3 operator-(const Vector3& _v2) const;
		
		Vector3 operator+(const Vector3& _v2) const;
		
		Vector3 elementMul(const Vector3& _v2) const;
		
		Vector3& operator=(const Vector3& _v1);
		
		double norm() const;
		
		double max() const;
		
		double sum() const;
		
		double dot(const Vector3& _v2) const;
		
		Vector3 sqrt();
		
		Vector3 rot(const Vector3& _p) const;
		
		Vector3 cross(const Vector3& _v2) const;

	  Vector3 abs() const {
		const Vector3& v = *this;
		return Vector3(::fabs(v[0]),::fabs(v[1]),::fabs(v[2]));
	  }
		
	  friend   std::ostream& operator<<(std::ostream& os, const Vector3& _v);
	};
	
	
	
	
	struct Quaternion {
		double q[4];
		
		Quaternion();
		
		Quaternion(double w, double x, double y, double z);
		
		Quaternion& operator=(const Quaternion& _q1);
		
		double operator[](int _idx) const;
		
		double& operator[](int _idx);
		
		Quaternion operator*(const Quaternion& _q2) const;
		
		Quaternion inv() const;
		
		Vector3 rotvec() const;
		
		static Quaternion quaternion(const Vector3& _v);
		
		Vector3 rot(const Vector3& _v) const;

	  friend std::ostream& operator<<(std::ostream& os, const Quaternion& _q);
	};
	
	
	
	struct Matrix3x3 {
		double m[3][3];
		
	  double operator()(int i, int j) const;


		Matrix3x3();

	  Matrix3x3(double m0, double m1, double m2);

		Matrix3x3(double m00, double m01, double m02,
				  double m10, double m11, double m12,
				  double m20, double m21, double m22);
		
		Matrix3x3& operator=(const Matrix3x3& _rhs);
		
		Matrix3x3 operator+(const Matrix3x3& _rhs) const;
		
		Matrix3x3 operator+(const Vector3& _rhs) const;
		
		Matrix3x3 operator-() const;
		
		Matrix3x3 operator-(const Matrix3x3& _q_tilde_2) const;
		
		Matrix3x3 operator*(const Matrix3x3& _m2) const;
		
		Matrix3x3 operator*(double _a) const;
		
		Vector3 operator*(const Vector3& _v) const;
		
		Matrix3x3 inv() const;
		
		double det() const;
		
		static Matrix3x3 identity();
		
		static Matrix3x3 diag(Vector3& _v);
		
	  friend   std::ostream& operator<<(std::ostream& os, const Matrix3x3& _v);
	};
	
	
	
	class coordinateTransform {
	public:
		coordinateTransform();

		void init();
		void reset() { mNeedsInit = true; }
		
		bool needsInit() { return mNeedsInit; }
		void setNeedsInit() { mNeedsInit = true; }
		
		void setOrigin(const Vector3& _v1, const Vector3& _v2);
		
		void updateScale(const Vector3& _p_tilde_1, const Vector3& _p_tilde_2);
		
		void updateRotation(const Quaternion& _q_tilde_1, const Quaternion& _q_tilde_2);
		
		void updateOffset(const Vector3& _p_tilde_1, const Vector3& _p_tilde_2,
						  const Quaternion& _q_tilde_1, const Quaternion& _q_tilde_2);

		Vector3 transformPosition(const Vector3& _v) const;
		Quaternion transformOrientation(const Quaternion& _v) const;

		void addBreadcrum( Vector3& _p1, Vector3& _p2 );
			
		Vector3 refStartPosition() const;
		void setRefStartPosition(Vector3 _pos);
  private:
		void measureScale(const Vector3& _p_tilde_1, const Vector3& _p_tilde_2,
						  const Vector3& _ref1, const Vector3& _ref2,
						  const Vector3& _var1, const Vector3& _var2, double gamma,
						  double& s_tilde, double& var_hat_s_tilde);
		
		void measureOffset(const Vector3& p_tilde_1, const Vector3& p_tilde_2,
						   double s_hat, const Vector3& r_hat,
						   const Vector3& sigma_p_1, const Vector3& sigma_p_2,
						   double sigma_hat_s, const Vector3& sigma_hat_r,
						   Vector3& t_tilde, Vector3& var_hat_t);
		
		
	public:
		// Start position
		std::vector<Vector3> p_1_0;
		std::vector<Vector3> p_2_0;
		int bestidx;

		volatile bool mNeedsInit;
		
		// Offset of position to world frame
		Vector3 t_hat;
		// Offset of orientation to world frame
		Vector3 r_hat;
		// Scale offset
		double s_hat;
		
		// Covariances
		Matrix3x3 Pr;
		Matrix3x3 Pt;
		double Ps;
		
		// Process noise
		Matrix3x3 Qt;
		Matrix3x3 Qr;
		double Qs;		
		
		// Position measurement noise
		Vector3 sigma_p_1;
		Vector3 sigma_p_2;
		// Rotational measurement noise
		Vector3 sigma_r_1;
		Vector3 sigma_r_2;
		
		double mGamma;
		
		double sigma_hat_s_hat;
		Vector3 sigma_hat_r_hat;
		
		Vector3 mRefStartPosition;
		
	};
	
} // namespace sefu

#endif // _COORDINATETRANSFORM_H_
