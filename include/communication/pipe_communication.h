/** \file pipe_communication.h
 *  \brief File implementing the class for pipe communication
 *  \author Niklas Bergström
 *
 *  \details
 */
#ifndef _PIPE_COMMUNICATION_H_
#define _PIPE_COMMUNICATION_H_

#include <iostream>

#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>

#include "communication/communication.h"

#include "parsers/command_parser.h"

#include "sf_exception.h"

/** \namespace sefu */
namespace sefu {
	
	using namespace pars;
	
	/** \namespace sefu::comm */
	namespace comm {
		
		// ================================================================================================
		// ========================================= PIPELISTENER =======================================
		// ================================================================================================
		/** \class pipeListener pipe_communication.h "pipe_communication.h"
		 *  \brief Class for incoming pipe communication
		 *  \author Niklas Bergström
		 *  \version 0.1
		 *  \ingroup Communication
		 *
		 *  \details Class implementing asyncronous listening to a pipe port. It will
		 *  read bytes from the port and pass them on to a processor. It is the task of
		 *  the processor to make sure communication is synchronized.
		 *
		 *  \todo Add methods for making sure that a whole packet is received before parsing
		 */
		template <class PARSER_T>
		class pipeListener : public listener<PARSER_T> {
			
		public:
			/** \brief Constructor
			 *
			 *  \details Takes a pipe port and a baudrate and creates a connection
			 *  to this port. Furthermore takes a processor and a filter for handling
			 *  of the incoming data.
			 *
			 *  \param _port The pipe id as a string
			 *  \param _baudRate The baud rate of the communication
			 *  \param _processor A processor for the incoming data
			 *  \param _filter A filter for the processed data
			 *
			 *  \throw An sfException if the connection could not be made
			 */
			pipeListener(const std::string& _id,
						 int _baudRate,
						 PARSER_T* _processor) :
			listener<PARSER_T>(_processor), mPipe(listener<PARSER_T>::mIOService)
			{
				
				mknod(_id.c_str(), S_IFIFO|0666, 0);
				
				FILE* pipe;
				
				int dev = open(_id.c_str(), O_RDONLY | O_NONBLOCK );
				pipe = fdopen(dev, "r");
				
				if( dev == -1 ) {
					std::cerr << "Error opening pipe " << _id << " for reading" << std::endl;
					exit(-1);
				}
				mPipe.assign(dev);
				
				startReceive();
			}
			
			
			
			/** \brief Virtual destructor */
			virtual ~pipeListener() {}
			
		protected:
			/** \brief Start receiving from the pipe port */
			virtual void startReceive()  {
				
				mPipe.async_read_some(boost::asio::buffer(this->mData,this->mBufferSize),
									  boost::bind(&listener<PARSER_T>::handleReceiveFrom, this,
												  boost::asio::placeholders::error,
												  boost::asio::placeholders::bytes_transferred));
			}
			
			
		protected:
			boost::asio::posix::stream_descriptor mPipe;
			
		};
		
		
		
		
		// ================================================================================================
		// ======================================== PIPETRANSMITTER =====================================
		// ================================================================================================
		/** \class pipeTransmitter pipe_communication.h "pipe_communication.h"
		 *  \brief Class for outgoing pipe communication
		 *  \author Niklas Bergström
		 *  \version 0.1
		 *  \ingroup Communication
		 *
		 *  \details Class implementing synchronous transmission to a pipe port. It will send either
		 *  a string or a chunk of data with specified size.
		 */
		class pipeTransmitter : public transmitter {
		public:
			/** \brief Constructor
			 *
			 *  \details Takes a pipe port and a baudrate and creates a connection
			 *  to this port.
			 *
			 *  \param _port The pipe port as a string
			 *  \param _baudRate The baud rate of the communication
			 *
			 *  \throw An sfException if the connection could not be made
			 */
			pipeTransmitter(const std::string& _id, int _baudRate) : transmitter(),  mPipe(mIOService)
			{
				mknod(_id.c_str(), S_IFIFO | 0666, 0);
				
				int dev = open(_id.c_str(), O_WRONLY);
				if( dev == -1 ) {
					std::cerr << "Error opening pipe " << _id << " for writing" << std::endl;
					exit(-1);
				}

				FILE* pipe;
				pipe = fdopen(dev, "w");
				
				mPipe.assign(dev);
			}
			
			// Documentation in superclass
			virtual bool sendString(const std::string& _message) {
				boost::system::error_code err;
				mPipe.write_some(boost::asio::buffer(_message.c_str(), _message.length()), err);
				if( err) {
					return false;
				}
				return true;
			}
			
			
			/** \brief Virtual destructor */
			virtual ~pipeTransmitter() {}
			
			
		public:
			// Documentation in superclass
			virtual bool sendRawBuffer( const void* _pkt, size_t _nbytes ) {
				boost::system::error_code err;
				
				mPipe.write_some(boost::asio::buffer(_pkt, _nbytes),err);
				if( err) {
					return false;
				}
				return true;
			}
			
		private:
			boost::asio::io_service mIOService;
			
			boost::asio::posix::stream_descriptor mPipe;
		};
		
		
	} // namespace comm
} // namespace sefu


#endif // _PIPE_COMMUNICATION_H_
