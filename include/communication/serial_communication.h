/** \file serial_communication.h
 *  \brief File implementing the class for serial communication
 *  \author Niklas Bergström
 *
 *  \details
 */
#ifndef _SERIAL_COMMUNICATION_H_
#define _SERIAL_COMMUNICATION_H_

#include <iostream>

#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>

#include "communication/communication.h"

#include "parsers/command_parser.h"

#include "sf_exception.h"

/** \namespace sefu */
namespace sefu {
    
    using namespace pars;
    
    /** \namespace sefu::comm */
    namespace comm {
        
        // ================================================================================================
        // ========================================= SERIALLISTENER =======================================
        // ================================================================================================
        /** \class serialListener serial_communication.h "serial_communication.h"
         *  \brief Class for incoming serial communication
         *  \author Niklas Bergström
         *  \version 0.1
         *  \ingroup Communication
         *
         *  \details Class implementing asyncronous listening to a serial port. It will
         *  read bytes from the port and pass them on to a processor. It is the task of
         *  the processor to make sure communication is synchronized.
         *
         *  \todo Add methods for making sure that a whole packet is received before parsing
         */
        template <class PACKET, class PARSER_T>
		class serialListener : public listener<PARSER_T> {
			
        public:
            /** \brief Constructor
             *
             *  \details Takes a serial port and a baudrate and creates a connection
             *  to this port. Furthermore takes a processor and a filter for handling
             *  of the incoming data.
             *
             *  \param _port The serial port as a string
             *  \param _baudRate The baud rate of the communication
             *  \param _processor A processor for the incoming data
             *  \param _filter A filter for the processed data
             *
             *  \throw An sfException if the connection could not be made
             */
            serialListener(const std::string& _port,
                           int _baudRate,
                           PARSER_T* _processor) :
			listener<PARSER_T>(_processor), mPort(listener<PARSER_T>::mIOService,_port)
            {
			  std::cout << "constructing serial listener\n";
                mPort.set_option(boost::asio::serial_port_base::baud_rate(_baudRate));
                
                startReceive();
            }
            
            
            // Documented in the superclass
            void handleReceiveFrom(const boost::system::error_code& _err,
                                     size_t _nbytes) {
                PACKET packet;
                
                bool gotPacket = parse_packet(&packet, (const uint8_t*)this->mData, _nbytes);
                if( gotPacket ) {
                    memcpy(this->mData, (const void*)&packet, sizeof(PACKET));
                    listener<PARSER_T>::parseWithProcessor(sizeof(PACKET));
                }
                
                startReceive();
            }
            
            
            /** \brief Virtual destructor */
            virtual ~serialListener() {}
            
        protected:
            /** \brief Start receiving from the serial port */
            virtual void startReceive()  {
                
                mPort.async_read_some(boost::asio::buffer(this->mData,
                                                          this->mBufferSize),
                                      boost::bind(&serialListener::handleReceiveFrom, this,
                                                  boost::asio::placeholders::error,
                                                  boost::asio::placeholders::bytes_transferred));
            }
            

		protected:
            //            boost::asio::io_service mIOService;
            boost::asio::serial_port mPort;
            
            
        private:
            // For computing checksum
            uint16_t checksum(const uint8_t* data, size_t length)
            {
                uint16_t sum = 0;
                
                for (size_t i = 0; i < length; ++i)
                    sum += data[i];
                
                return sum;
            }
            
            
            // Parses a serial message
            bool parse_packet(PACKET* const _packet, const uint8_t* const rx_buffer, const size_t num_bytes_received) {
                
                const uint8_t packet_size = sizeof(PACKET);
                
                static union bus {
                    PACKET packet;
                    uint8_t bytes[packet_size];
                } rx;
                
                static int num_bytes_stored = -1;
                
                for( int i = 0; (i < num_bytes_received ) && (num_bytes_stored < packet_size); ++i) {
                    if ((num_bytes_stored > 0) || (rx_buffer[i] == 0xE5)) {
                        rx.bytes[num_bytes_stored++] = rx_buffer[i];
                    }
                }
                
                if( num_bytes_stored == packet_size)
                {
                    num_bytes_stored = 0;
                    if( checksum(rx.bytes, packet_size - 2) == rx.packet.Checksum )
                    {
                        *_packet = rx.packet;
                        return true;
                    }
                    else
                    {
					  //std::cout << "Checksum error." << std::endl;
                    }
                }
                
                return false;
            }
            
        };
        
        
        
        // ================================================================================================
        // ======================================== SERIALTRANSMITTER =====================================
        // ================================================================================================
        /** \class serialTransmitter serial_communication.h "serial_communication.h"
         *  \brief Class for outgoing serial communication
         *  \author Niklas Bergström
         *  \version 0.1
         *  \ingroup Communication
         *
         *  \details Class implementing synchronous transmission to a serial port. It will send either
         *  a string or a chunk of data with specified size.
         */
        class serialTransmitter : public transmitter {
        public:
            /** \brief Constructor
             *
             *  \details Takes a serial port and a baudrate and creates a connection
             *  to this port.
             *
             *  \param _port The serial port as a string
             *  \param _baudRate The baud rate of the communication
             *
             *  \throw An sfException if the connection could not be made
             */
            serialTransmitter(const std::string& _port, int _baudRate) : transmitter(), mPort(mIOService)
            {
                boost::system::error_code err;
                mPort.open(_port, err);
                mPort.set_option(boost::asio::serial_port_base::baud_rate(_baudRate));
            }
            
            
            // Documentation in superclass
            virtual bool sendString(const std::string& _message) {
                boost::system::error_code err;
                mPort.write_some(boost::asio::buffer(_message.c_str(), _message.length()), err);
                if( err) {
                    return false;
                }
                return true;
            }
            
            
            /** \brief Virtual destructor */
            virtual ~serialTransmitter() {}
            
            
        public:
            // Documentation in superclass
            virtual bool sendRawBuffer( const void* _pkt, size_t _nbytes ) {
                boost::system::error_code err;

				std::cout << "sending " << ((char*)_pkt)[0] << " to ptam\n";

                mPort.write_some(boost::asio::buffer(_pkt, _nbytes),err);
                if( err) {
                    return false;
                }
                return true;
            }
            
        private:
            boost::asio::io_service mIOService;
            boost::asio::serial_port mPort;
        };
        
        
        
        // ================================================================================================
        // ========================================== SERIALDUPLEX ========================================
        // ================================================================================================
        /** \class serialDuplex serial_communication.h "serial_communication.h"
         *  \brief Class for outgoing and incoming serial communication
         *  \author Niklas Bergström
         *  \version 0.1
         *  \ingroup Communication
         *
         *  \details Since two connections cannot simultaneously be opened to the serial port,
         *  if we want to send and receive over the serial port, a \c serialDuplex instance must
         *  be created. It inherits from both \c serialListener and \c transmitter.
         */
        template <class PACKET, class PARSER_T>
        class serialDuplex : public listener<PARSER_T>, public transmitter {
			
		public:
			/** \brief Constructor
			 *
			 *  \details Takes a serial port and a baudrate and creates a connection
			 *  to this port. Furthermore takes a processor and a filter for handling
			 *  of the incoming data.
			 *
			 *  \param _port The serial port as a string
			 *  \param _baudRate The baud rate of the communication
			 *  \param _processor A processor for the incoming data
			 *  \param _filter A filter for the processed data
			 *
			 *  \throw An sfException if the connection could not be made
			 */
			serialDuplex(const std::string& _port,
						   int _baudRate,
						   PARSER_T* _processor) :
			listener<PARSER_T>(_processor), transmitter(), mPort(listener<PARSER_T>::mIOService,_port)
			{
				std::cout << "constructing serial listener\n";
				mPort.set_option(boost::asio::serial_port_base::baud_rate(_baudRate));
				
				startReceive();
			}
			

			
			
			// Documented in the superclass
			void handleReceiveFrom(const boost::system::error_code& _err,
								   size_t _nbytes) {
				PACKET packet;
				
				bool gotPacket = parse_packet(&packet, (const uint8_t*)this->mData, _nbytes);
				if( gotPacket ) {
					memcpy(this->mData, (const void*)&packet, sizeof(PACKET));
					listener<PARSER_T>::parseWithProcessor(sizeof(PACKET));
				}
				
				startReceive();
			}
			
			
			/** \brief Virtual destructor */
			virtual ~serialDuplex() {}
			
		protected:
			/** \brief Start receiving from the serial port */
			virtual void startReceive()  {
				
				mPort.async_read_some(boost::asio::buffer(this->mData,
														  this->mBufferSize),
									  boost::bind(&serialDuplex::handleReceiveFrom, this,
												  boost::asio::placeholders::error,
												  boost::asio::placeholders::bytes_transferred));
			}
			
			
		protected:
			//            boost::asio::io_service mIOService;
			boost::asio::serial_port mPort;
			
			
		private:
			// For computing checksum
			uint16_t checksum(const uint8_t* data, size_t length)
			{
				uint16_t sum = 0;
				
				for (size_t i = 0; i < length; ++i)
					sum += data[i];
				
				return sum;
			}
			
			
			// Parses a serial message
			bool parse_packet(PACKET* const _packet, const uint8_t* const rx_buffer, const size_t num_bytes_received) {
				
				const uint8_t packet_size = sizeof(PACKET);
				
				static union bus {
					PACKET packet;
					uint8_t bytes[packet_size];
				} rx;
				
				static int num_bytes_stored = -1;
				
				for( int i = 0; (i < num_bytes_received ) && (num_bytes_stored < packet_size); ++i) {
					if ((num_bytes_stored > 0) || (rx_buffer[i] == 0xE5)) {
						rx.bytes[num_bytes_stored++] = rx_buffer[i];
					}
				}
				
				if( num_bytes_stored == packet_size)
				{
					num_bytes_stored = 0;
					if( checksum(rx.bytes, packet_size - 2) == rx.packet.Checksum )
					{
						*_packet = rx.packet;
						return true;
					}
					else
					{
						//std::cout << "Checksum error." << std::endl;
					}
				}
				
				return false;
			}
			
			
			// Documentation in superclass
			virtual bool sendString(const std::string& _message) {
				boost::system::error_code err;
				mPort.write_some(boost::asio::buffer(_message.c_str(), _message.length()), err);
				if( err) {
					return false;
				}
				return true;
			}
			
			
		public:
			// Documentation in superclass
			virtual bool sendRawBuffer( const void* _pkt, size_t _nbytes ) {
				boost::system::error_code err;
				
				std::cout << "sending " << ((char*)_pkt)[0] << " to ptam\n";
				
				mPort.write_some(boost::asio::buffer(_pkt, _nbytes),err);
				if( err) {
					return false;
				}
				return true;
			}
                        
        };
        
    } // namespace comm
} // namespace sefu


#endif // _SERIAL_COMMUNICATION_H_
