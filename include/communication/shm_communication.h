/** \file shm_communication.h
 *  \brief File implementing the class for interprocess communication
 *  \author Niklas Bergström
 *
 *  \details
 */
#ifndef _SHM_COMMUNICATION_H_
#define _SHM_COMMUNICATION_H_

#include <iostream>

#include "communication/communication.h"

#include <boost/interprocess/sync/interprocess_semaphore.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>


/** namespace sefu */
namespace sefu {
    /** namespace sefu::comm */
    namespace comm {
        
        using namespace boost::interprocess;
        
        /** \struct shared_memory_buffer
         *  \brief Struct for the interprocess communication
         *  \author Niklas Bergström
         *  \ingroup Communication
         *
         *  \details This templated struct is used for the interprocess
         *  communication. The same struct must be used in the communicating
         *  process as well.
         *
         *  \tparam PACKET The packet which to send between the processes
         *
         *  \todo Restrict communication using more semaphores
         *  \todo Move this to a header to be included in the communicating process
         */
        template <class PACKET>
        struct shared_memory_buffer {
            shared_memory_buffer() : reader(1) {
                memset(&mPacket,0,sizeof(PACKET));
            }
            
            boost::interprocess::interprocess_semaphore reader;
            
            PACKET mPacket;
        };
        
        
        
        /** \class shmTransmitter
         *  \brief Class for transmission through shared memory
         *  \author Niklas Bergström
         *  \ingroup Communication
         *
         *  \details This templated class is used for the interprocess
         *  communication. It inherits from \c transmitter, but does not
         *  implement the \fn sendString function
         *
         *  \tparam PACKET The packet which to send between the processes
         */
        template <class PACKET>
        class shmTransmitter : public transmitter {
        public:
            shmTransmitter( const std::string& _shm_id, int _size ) : transmitter() {
                // Setup pool with shared memory
                shm = boost::shared_ptr<shared_memory_object>( new shared_memory_object(open_or_create, _shm_id.c_str(), read_write) );
                
                shm->truncate(sizeof(shared_memory_buffer<PACKET>));
                
                region = boost::shared_ptr<mapped_region>( new mapped_region(*shm,read_write) );
                
                addr = region->get_address();
                
                data = new (addr) shared_memory_buffer<PACKET>;
            }
            
            
            
            virtual bool sendString( const std::string& _message ) {
                std::cout << "Sending strings is not implemented for interprocess communication\n";
                return false;
            }
            
            virtual ~shmTransmitter() {}
            
            
        protected:
            virtual bool sendRawBuffer( const void* _pkt, size_t _nbytes ) {
                memcpy(&(data->mPacket), _pkt, _nbytes);

				data->reader.post();
				
                return true;
            }
            
        private:
            // Shared memory
            boost::shared_ptr<shared_memory_object> shm;
            boost::shared_ptr<mapped_region> region;
            void *addr;
            shared_memory_buffer<PACKET>* data;
        };
        
    } // namespace comm
} // namespace sefu

#endif // _SHM_COMMUNICATION_H_
