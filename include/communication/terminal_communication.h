/** \file terminal_communication.h
 *  \brief File implementing the class for terminal communication
 *  \author Niklas Bergström
 *
 *  \details
 */
#ifndef _TERMINAL_COMMUNICATION_H_
#define _TERMINAL_COMMUNICATION_H_

#include <iostream>

#include <boost/asio.hpp>
#include <boost/bind.hpp>

#include <boost/shared_ptr.hpp>
#include <boost/interprocess/shared_memory_object.hpp>

#include "communication/communication.h"

/** \namespace sefu */
namespace sefu {
	
	using namespace pars;
	
	/** \namespace sefu::comm */
	namespace comm {
		

		template <class PARSER_T>
		class terminalListener : public listener<PARSER_T>, public boost::enable_shared_from_this<terminalListener<PARSER_T> > {
			
		public:
			typedef boost::shared_ptr<terminalListener> terminalListener_t;
			
			
			static void create( boost::asio::io_service& _service ) {
				terminalListener_t input( new terminalListener( _service ) );

				input->startReceive();
			}
			
			
			explicit terminalListener(PARSER_T* _processor) : listener<PARSER_T>(_processor),
			mInput(this->mIOService) {
				mInput.assign(STDIN_FILENO);
			}
			
			
			virtual ~terminalListener() {}
			
		protected:
			void startReceive() {
				boost::asio::async_read(mInput,
										boost::asio::buffer(this->mData, this->mBufferSize ),
										boost::bind(&listener<PARSER_T>::handleReceiveFrom,
													this->shared_from_this(),
													boost::asio::placeholders::error,
													boost::asio::placeholders::bytes_transferred));
			}
			
			
		private:
			boost::asio::posix::stream_descriptor mInput;
		};
		
		
		
		using namespace boost::asio::ip;
		

		
		

	} // namespace comm
} // namespace sefu

#endif // _TERMINAL_COMMUNICATION_H_
