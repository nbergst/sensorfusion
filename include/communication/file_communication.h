/** \file file_communication.h
 *  \brief File implementing the class for file communication
 *  \author Niklas Bergström
 *
 *  \details
 */
#ifndef _FILE_COMMUNICATION_H_
#define _FILE_COMMUNICATION_H_

#include <iostream>
#include <fstream>

#include <boost/shared_ptr.hpp>

#include "communication/communication.h"

/** \namespace sefu */
namespace sefu {
	
	using namespace pars;
	
	/** \namespace sefu::comm */
	namespace comm {
		
		// ================================================================================================
		// ========================================== UDPLISTENER =========================================
		// ================================================================================================
		/** \class fileListener file_communication.h "file_communication.h"
		 *  \brief Class for incoming file communication
		 *  \author Niklas Bergström
		 *  \version 0.1
		 *  \ingroup Communication
		 *
		 *  \details Class implementing asyncronous listening to a upd socket. It will
		 *  read bytes from the port and pass them on to a processor. It is the task of
		 *  the processor to make sure communication is synchronized.
		 */
//		class fileListener : public listener {
//			
//		public:
//			/** \brief Constructor
//			 *
//			 *  \details Takes a file address and a port number and creates a connection
//			 *  which receives multicasts from any address.
//			 *  Furthermore takes a processor and a filter for handling
//			 *  of the incoming data.
//			 *
//			 *  \param _address The ip address (IPv4)
//			 *  \param _port The port of the communication
//			 *  \param _processor A processor for the incoming data
//			 *  \param _filter A filter for the processed data
//			 *
//			 *  \throw An sfException if the connection could not be made
//			 */
//			fileListener(const std::string& _address,
//						int _port,
//						processor* _processor) : listener(_processor),
//			mSocket(mIOService)
//			{
//				// Open endpoint on ip v4 and _port
//				file::endpoint endp(file::v4(), _port);
//				mSocket.open(endp.protocol());
//				
//				// Allow for multiple connections
//				mSocket.set_option(boost::asio::ip::file::socket::reuse_address(true));
//				mSocket.bind(endp);
//				
//				// Join the multicast group.
//				mSocket.set_option(boost::asio::ip::multicast::join_group(address::from_string(_address)));
//				
//				startReceive();
//			}
//			
//			
//			
//			/** \brief Constructor
//			 *
//			 *  \details Takes a file address and a port number and creates a connection
//			 *  to receive from a specified ip
//			 *  Furthermore takes a processor and a filter for handling
//			 *  of the incoming data.
//			 *
//			 *  \param _listenAddress The ip address from which to receive (IPv4)
//			 *  \param _address The ip address (IPv4)
//			 *  \param _port The port of the communication
//			 *  \param _processor A processor for the incoming data
//			 *  \param _filter A filter for the processed data
//			 *
//			 *  \throw An sfException if the connection could not be made
//			 */
//			fileListener(const boost::asio::ip::address& _listenAddress,
//						const boost::asio::ip::address& _address,
//						int _port,
//						processor* _processor) : listener(_processor),
//			mSocket(mIOService)
//			{
//				file::endpoint endp(_listenAddress, _port);
//				mSocket.open(endp.protocol());
//				
//				mSocket.set_option(boost::asio::ip::file::socket::reuse_address(true));
//				mSocket.bind(endp);
//				
//				// Join the multicast group.
//				mSocket.set_option(boost::asio::ip::multicast::join_group(_address));
//				
//				startReceive();
//			}
//			
//			
//			/** \brief Virtual destructor */
//			virtual ~fileListener() {}
//			
//		protected:
//			/** \brief Start receiving from the file port */
//			virtual void startReceive() {
//				mSocket.async_receive_from(boost::asio::buffer(listener::mData,
//															   listener::mBufferSize), mSenderEndpoint,
//										   boost::bind(&listener::handleReceiveFrom, this,
//													   boost::asio::placeholders::error,
//													   boost::asio::placeholders::bytes_transferred));
//			}
//			
//			
//			
//		private:
//			// Socket
//			boost::asio::ip::file::socket mSocket;
//			// Where to send
//			boost::asio::ip::file::endpoint mSenderEndpoint;
//		};
		
		
		
		// ================================================================================================
		// ========================================= UDPTRANSMITTER =======================================
		// ================================================================================================
		/** \class fileTransmitter file_communication.h "file_communication.h"
		 *  \brief Class for outgoing file communication
		 *  \author Niklas Bergström
		 *  \version 0.1
		 *  \ingroup Communication
		 *
		 *  \details Class implementing synchronous transmission to a file socket. It will send either
		 *  a string or a chunk of data with specified size.
		 */
		class fileTransmitter : public transmitter {
		public:
			/** \brief Constructor
			 *
			 *  \details Takes an ip address and a port and creates a connection.
			 *
			 *  \param _address The ip address (IPv4)
			 *  \param _port The port of the communication
			 *
			 *  \throw An sfException if the connection could not be made
			 */
			fileTransmitter(const std::string& _name,
							int _port) : transmitter(),
			mFile(_name.c_str(), std::ofstream::out)
			{
			}
			
			
			// Documentation in superclass
			virtual bool sendString( const std::string& _message ) {
				mFile << _message << std::endl;
				
				return true;
			}
			
			/** \brief Virtual destructor */
			virtual ~fileTransmitter() {
				std::cout << "closing file\n";
				mFile.close();
			}
			
		protected:
			// Documentation in superclass
			virtual bool sendRawBuffer( const void* _pkt, size_t _nbytes ) {
				
				char* tosend = new char[_nbytes+1];
				memcpy(tosend,_pkt,_nbytes);
				tosend[_nbytes] = '\0';
				
				mFile << tosend;
				
				return true;
			}
			
		private:
			std::ofstream mFile;
			
		};
		
	} // namespace comm
} // namespace sefu

#endif // _FILE_COMMUNICATION_H_
