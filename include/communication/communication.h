/** \file communication.h
 *  \brief Header containing base classes for communication
 *  \author Niklas Bergström
 *
 *  \details There are two main classes for communication: listener
 *  and transmitter. They contain the structure needed for subclasses
 *  that wish to implement communication protocols such as serial
 *  or udp.
 */
#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_

#include "parsers/processor.h"
#include "parsers/processor_callbacks.h"
#include "filters/filter.h"

#include <boost/asio.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/system/error_code.hpp>
#include <boost/thread.hpp>
#include <boost/variant.hpp>

#include <vector>

namespace sefu {

    class sensor;
    
    namespace comm {

        class communicator {
        public:
            virtual ~communicator() {}
            
        };

        /** \class listener listener.h "listener.h"
         *  \brief Base class for incoming communication
         *  \author Niklas Bergström
         *  \version 0.1
         *  \ingroup Communication
         *
         *  \details The listener class contains methods for asynchronously handling
         *  receiving of messages from external sources. Needs to be subclassed.
         *
         */
		template <class PARSER_T>
        class listener : virtual public communicator {
        public:
            
            /** \brief Adds callbacks
             *
             *  \details Whenever a full message has arrived and been parsed
             *  the listener will call registered callbacks
             */
			virtual void addCallback( boost::function< void(PARSER_T*)> _clbk ) {
                mCallbacks.push_back(_clbk);
            }
            
            
            /** \brief Callback for new data
             *
             *  \details This function should be called by a subclass once new data
             *  has arrived and put into mData. The function will use the processor
             *  instance to parse the data, and call callbacks depending on the result
             *  of the parsing. After this the new data will be passed to the filter
             *  which is responsible for filtering the raw data.
             *
             *  \sa filter
             */
            virtual void handleReceiveFrom(const boost::system::error_code& _err,
					   size_t _nbytes) {
                parseWithProcessor(_nbytes);
                
                // Wait for the next incoming data
                startReceive();
            }


            void parseWithProcessor(size_t _nbytes) {
				if(mProcessor->parse(mData,_nbytes)) {
                    for( int i=0; i<mCallbacks.size(); ++i ) {
                        mCallbacks[i](&*mProcessor);
                    }
                }
            }

	    
            
            /** \brief Starts listening in new thread */
            void start() {
                if (mThread) {
                    return;
                }
				mThread.reset(new boost::thread(boost::bind(&boost::asio::io_service::run, &mIOService)));
            }

            
            
            /** \brief Stops listening and joins thread */
            void stop() {
                if (!mThread) {
                    return;
                }
                
                mIOService.stop();
                mThread->join();
                mIOService.reset();
                mThread.reset();
            }

            
            
            /** \brief Sensor is a friend */
            friend sensor;
            
            
            /** \brief Virtual destructor */
            virtual ~listener() {}
            
            
        protected:
            /** \brief Constructor
             *
             *  \details This class shouldn't be instantiated, so the constructor
             *  is protected. It takes a processor subclass, which will be responsible
             *  for parsing and holding the data retrieved by the listener. The provided
             *  filter is responsible for keeping its own data structure and to filter
             *  the incoming data appropriately
             *
             *  \param _processor An instance of a processor subclass
             *  \param _filter An instance of a filter subclass
             *
             *  \sa processor
             */
            listener( PARSER_T* _processor  ) : mProcessor(_processor) {}
			
            
            /** \brief Start receiving data */
            virtual void startReceive() {}
            
            
        protected:
            // Handles io communication
            boost::asio::io_service mIOService;
            
            // Thread for the io
            boost::scoped_ptr<boost::thread> mThread;
            
            // Is called once an entire message is received
	    std::vector<boost::function<void (PARSER_T*)> > mCallbacks;
            
            // Parses incoming data
	    boost::shared_ptr<PARSER_T> mProcessor;
            
            static const unsigned int mBufferSize = 4096;
            // Buffer that holds incoming data
            char mData[mBufferSize];
        };
        
        
        
        /** \class transmitter transmitter.h "transmitter.h"
         *  \brief Base class for outgoing communication
         *  \author Niklas Bergström
         *  \version 0.1
         *  \ingroup Communication
         *
         *  \details The transmitter class is the base class for synchronously
         *  transmitting messages via different protocols implemented by a
         *  inheriting class.
         */
        class transmitter : virtual public communicator {
        public:
            
            /** \brief Sends string message
             *
             *  \details Method for sending a message as a string. If the subclass
             *  does not implement this method, false will be returned. If the subclass
             *  does implement this method, it should return true if the message has
             *  been successfully sent.
             *
             *  \param _message String to send
             *  \return true if the message was successfully sent
             */
            virtual bool sendString( const std::string& _message ) { return false; };
            
			
			/** \brief Sends string message
			 *
			 *  \details Method for sending a message as a string. If the subclass
			 *  does not implement this method, false will be returned. If the subclass
			 *  does implement this method, it should return true if the message has
			 *  been successfully sent.
			 *
			 *  \param _message stringstream to send
			 *  \return true if the message was successfully sent
			 */
			bool sendString( const std::stringstream& _message ) {
				return sendString(_message.str());
			}
			
            /** \brief Sends raw message
             *
             *  \details Method for sending a raw message. This method is not virtual,
             *  so the subclass should rather override sendRawBuffer instead
             *
             *  \param _pkt The packet to send
             *  \return true if the packet was successfully sent
             */
            template <class T>
            bool sendRaw( const T& _pkt ) {
                bool r = sendRawBuffer(&_pkt, sizeof(T));
                return r;
            }
            
            
            /** \brief Virtual destructor */
            virtual ~transmitter() {}
            
            
        protected:
            /** \brief Sends raw message
             *
             *  \details This method is called by sendRaw, and should be implemented by
             *  any subclass that wants to send raw messages.
             *
             *  \param _pkt A pointer to the data to send
             *  \param _nbytes The number of bytes to send
             *  \return true if the packet was successfully sent
             */
            virtual bool sendRawBuffer( const void* _pkt, size_t _nbytes ) {
                std::cout << "Sending raw buffer from base class (does nothing)\n";
                return false;
            }
            
        };
        
    } // namespace comm
} // namespace sefu

#endif // _COMMUNICATION_H_
