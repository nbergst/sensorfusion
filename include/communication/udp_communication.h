/** \file udp_communication.h
 *  \brief File implementing the class for udp communication
 *  \author Niklas Bergström
 *
 *  \details
 */
#ifndef _UDP_COMMUNICATION_H_
#define _UDP_COMMUNICATION_H_

#include <iostream>

#include <boost/asio.hpp>
#include <boost/bind.hpp>

#include <boost/shared_ptr.hpp>

#include "communication/communication.h"

/** \namespace sefu */
namespace sefu {
    
    using namespace pars;
    
    /** \namespace sefu::comm */
    namespace comm {
        
        using namespace boost::asio::ip;
        
        // ================================================================================================
        // ========================================== UDPLISTENER =========================================
        // ================================================================================================
        /** \class udpListener udp_communication.h "udp_communication.h"
         *  \brief Class for incoming udp communication
         *  \author Niklas Bergström
         *  \version 0.1
         *  \ingroup Communication
         *
         *  \details Class implementing asyncronous listening to a upd socket. It will
         *  read bytes from the port and pass them on to a processor. It is the task of
         *  the processor to make sure communication is synchronized.
         */
		template <class PARSER_T>
        class udpListener : public listener<PARSER_T> {
            
        public:
            /** \brief Constructor
             *
             *  \details Takes a udp address and a port number and creates a connection
             *  which receives multicasts from any address.
             *  Furthermore takes a processor and a filter for handling
             *  of the incoming data.
             *
             *  \param _address The ip address (IPv4)
             *  \param _port The port of the communication
             *  \param _processor A processor for the incoming data
             *  \param _filter A filter for the processed data
             *
             *  \throw An sfException if the connection could not be made
             */
            udpListener(const std::string& _address,
                        int _port,
                        PARSER_T* _processor) : listener<PARSER_T>(_processor),
            mSocket(this->mIOService)
            {
                // Open endpoint on ip v4 and _port
                udp::endpoint endp(udp::v4(), _port);
                mSocket.open(endp.protocol());
                
                // Allow for multiple connections
                mSocket.set_option(boost::asio::ip::udp::socket::reuse_address(true));
                mSocket.bind(endp);
				
				if( address::from_string(_address).is_multicast() ) {
					// Join the multicast group.
					mSocket.set_option(boost::asio::ip::multicast::join_group(address::from_string(_address)));
				}
				
                startReceive();
            }

            

            /** \brief Constructor
             *
             *  \details Takes a udp address and a port number and creates a connection
             *  to receive from a specified ip
             *  Furthermore takes a processor and a filter for handling
             *  of the incoming data.
             *
             *  \param _listenAddress The ip address from which to receive (IPv4)
             *  \param _address The ip address (IPv4)
             *  \param _port The port of the communication
             *  \param _processor A processor for the incoming data
             *  \param _filter A filter for the processed data
             *
             *  \throw An sfException if the connection could not be made
             */
            udpListener(const boost::asio::ip::address& _listenAddress,
                        const boost::asio::ip::address& _address,
                        int _port,
                        processor* _processor) : listener<PARSER_T>(_processor),
            mSocket(this->mIOService)
            {
                udp::endpoint endp(_listenAddress, _port);
                mSocket.open(endp.protocol());
                
                mSocket.set_option(boost::asio::ip::udp::socket::reuse_address(true));
                mSocket.bind(endp);
                
                // Join the multicast group.
                mSocket.set_option(boost::asio::ip::multicast::join_group(_address));
                
                startReceive();
            }

            
            /** \brief Virtual destructor */
            virtual ~udpListener() {}
            
        protected:
            /** \brief Start receiving from the udp port */
            virtual void startReceive() {
                mSocket.async_receive_from(boost::asio::buffer(this->mData,
                                                               this->mBufferSize), mSenderEndpoint,
                                           boost::bind(&listener<PARSER_T>::handleReceiveFrom, this,
                                                       boost::asio::placeholders::error,
                                                       boost::asio::placeholders::bytes_transferred));
            }

            
            
        private:
            // Socket
            boost::asio::ip::udp::socket mSocket;
            // Where to send
            boost::asio::ip::udp::endpoint mSenderEndpoint;
        };

        
        
        // ================================================================================================
        // ========================================= UDPTRANSMITTER =======================================
        // ================================================================================================
        /** \class udpTransmitter udp_communication.h "udp_communication.h"
         *  \brief Class for outgoing udp communication
         *  \author Niklas Bergström
         *  \version 0.1
         *  \ingroup Communication
         *
         *  \details Class implementing synchronous transmission to a udp socket. It will send either
         *  a string or a chunk of data with specified size.
         */
        class udpTransmitter : public transmitter {
        public:
            /** \brief Constructor
             *
             *  \details Takes an ip address and a port and creates a connection.
             *
             *  \param _address The ip address (IPv4)
             *  \param _port The port of the communication
             *
             *  \throw An sfException if the connection could not be made
             */
            udpTransmitter(const std::string& _address,
                           int _port) : transmitter(),
            mSocket(mIOService)
            {
                mEndpoint = udp::endpoint(address::from_string(_address),_port);
                
                mSocket.open(boost::asio::ip::udp::v4());
                
                mSocket.set_option(boost::asio::ip::udp::socket::reuse_address(true));
				if( address::from_string(_address).is_multicast() ) {
					mSocket.set_option(boost::asio::socket_base::broadcast(true));
				}
            }
            
            
            // Documentation in superclass
            virtual bool sendString( const std::string& _message ) {
                mSocket.send_to(boost::asio::buffer(_message), mEndpoint);
				
                return true;
            }
            
            /** \brief Virtual destructor */
            virtual ~udpTransmitter() {}
            
        protected:
            // Documentation in superclass
            virtual bool sendRawBuffer( const void* _pkt, size_t _nbytes ) {
                mSocket.send_to(boost::asio::buffer(_pkt,_nbytes), mEndpoint);
                
                return true;
            }
            
        private:
            boost::asio::io_service mIOService;
            boost::asio::ip::udp::socket mSocket;
            boost::asio::ip::udp::endpoint mEndpoint;
        };
        
    } // namespace comm
} // namespace sefu

#endif // _UDP_COMMUNICATION_H_
