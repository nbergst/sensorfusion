#ifndef _SENSORFUSION_H_
#define _SENSORFUSION_H_

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include <boost/interprocess/sync/interprocess_semaphore.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>

#include <fstream>

#include "sensor.h"
#include "types.h"
#include "parsers/command_parser.h"

namespace sefu {
	
	class sensorFusion {
		
		enum eFusionMode {
			viconOnlyMode = 0,
			ptamOnlyMode,
			odoOnlyMode,
			fullFusionMode
		};
		
		
	public:
		// Constructor
		sensorFusion(const std::string& _configFile);
		
		// Destructor
		~sensorFusion() {
		}
		
		
		// For running as thread
		void operator()();
		
		void transmitToVehicle() const;
		
		bool logToGUI() const;
		bool logToFile() const;
		
		// Returns the sensor with a specified id
		boost::shared_ptr<sensor> sensorWithID( const std::string& _id ) const;
		
		// Stops the tread
		void stop();
		
		bool running() { return mRunning; }
		
		// Loops for different fusion modes
		void viconMode();
		void ptamMode();
		void odoMode();
		
		void singleSensorMode(const std::string& _s);
		int full(int state);
		sensor_t full(sensor_t _current);
		
		void toggleQuality( char _c );
		
		void enableLocalEcho() { mLocalEcho = true; }
		void disableLocalEcho() { mLocalEcho = false; }
		void toggleLocalEcho() { mLocalEcho = !mLocalEcho;
			if( mLocalEcho ) {
				std::cout << "Local Echo ON\n";
			} else {
				std::cout << "Local Echo OFF\n";
			}
		}
		void toggleUIOutput() {
			mDisplayTransformed = !mDisplayTransformed;
		}
		
		
		void setViconMode();
		void setPTAMMode();
		void setOdometryMode();
		void setPTAMPriorityMode();
		void setFullFusion();
		
		void resetPTAM();
		void resetTransformations();
		
		void processCommand(char _message);
		
		
		
	private:
		// Packet to send
		VehiclePacket_t mToSend;
		
		// Number of attached sensors
		int mNSensors;
		
		// Sensor ids
		std::vector<std::string> mSensorIDs;
		
		// Attached sensors
		std::vector<boost::shared_ptr<sensor> > mSensors;
		
		// Name of vehicle
		std::string mName;
		
		// What mode to use for the fusion
		volatile eFusionMode mMode;
		
		// Indicates whether fusion is running
		volatile bool mRunning;
		
		// Transmitter to vehicle
		boost::shared_ptr<comm::transmitter> mVehicleTransmitter;
		
		// Transmitter for logging
		boost::shared_ptr<comm::transmitter> mGUILogger;
		
		boost::shared_ptr<comm::transmitter> mFileLogger;
		
		boost::shared_ptr<comm::listener<sefu::command_parser> > mCommandReceiver;
		
		// Echo log text to terminal
		volatile bool mLocalEcho;
		
		// Display transformed or raw positions
		volatile bool mDisplayTransformed;
		
		// Pointer to the active sensor
		sensor_t mActiveSensor;

		bool mForcePTAM;
		bool mForceOdo;
		
	};
	
} // namespace sefu

#endif // _SENSORFUSION_H_
