/** \file communications.h
 *  \brief Convenience header for communication related classes
 *  \author Niklas Bergström
 *
 *  \details
 */
#ifndef _COMMUNICATIONS_H_
#define _COMMUNICATIONS_H_

#include "communication/serial_communication.h"
#include "communication/shm_communication.h"
#include "communication/udp_communication.h"
#include "communication/file_communication.h"
#include "communication/terminal_communication.h"
#include "communication/pipe_communication.h"

#include "parsers/command_parser.h"

/** \defgroup Communication Sensor Fusion Communication
 *  This group contains classes related to in- and outgoing
 *  communication with sensors or other processes.
 */

/** namespace sefu */
namespace sefu {
	
	using namespace pars;
	using namespace filt;
	
	/** namespace sefu::comm */
	namespace comm {
		
		/** \fn template <class L, class P, class F> boost::shared_ptr<L> getListener( const std::string& _address, short _port, const std::string& _name )
		 *  \brief Returns an initialized listener
		 *  \ingroup Communication
		 *
		 *  \details Given, depending on the listener type, an address/port/name, a port number/baud rate, and the name of the vehicle, the function will return an initialized listener.
		 *
		 *  \tparam L The listener class
		 *  \tparam P The processor class
		 *  \tparam F The filter class
		 *
		 *  \param _address Address/port/name of the communicator
		 *  \param _port Port/Baudrate etc of the communicator
		 *  \param _name Name of the vehicle
		 *  \return An initialized listener
		 */
		template <class L, class P, class F, class PACKET>
		boost::shared_ptr<L> getListener( const std::string& _address, short _port, const std::string& _name ) {
			
			try {
				P* p = new P(_name);
				F* f = new F();
				boost::shared_ptr<L> l( new L(_address, _port, p, f) );
				return l;
			}
			catch  (std::exception& e) {
				std::cerr << e.what() << std::endl;
			}
			
			return boost::shared_ptr<L>();
		}
		
		
		/** \fn listener* getListener( const std::string& _type, const std::string& _address, short _port, const std::string& _name, processor* _processor, filter* _filter )
		 *  \brief Returns an initialized listener
		 *  \ingroup Communication
		 *
		 *  \details Given, depending on the listener type, an address/port/name, a port number/baud rate, and the name of the vehicle, the function will return an initialized listener.
		 *
		 *  \param _type Type of communication (udp, serial etc.)
		 *  \param _address Address/port/name of the communicator
		 *  \param _port Port/Baudrate etc of the communicator
		 *  \param _name Name of the vehicle
		 *  \param _processor An initialized processor
		 *  \param _filter An initialized processor
		 *  \return An initialized listener
		 */
		template <class PARSER_T>
		listener<PARSER_T>* getListener(const std::string& _type, // type of communication
										const std::string& _address, int _port,
										const std::string& _name, // name of sensor
										PARSER_T* _processor) {
			
			listener<PARSER_T>* l = NULL;
			
			try {
				if( !_type.compare("udp") ) {
					l = new udpListener<PARSER_T>(_address, _port, dynamic_cast<PARSER_T*>(_processor));
				} else if( !_type.compare("serial") ) {
					if( !_name.compare("ptam") ) {
						l = new serialListener<ptamProcessor::PTAMPacket_t,PARSER_T>(_address, _port, dynamic_cast<processor*>(_processor));
					} else if( !_name.compare("vicon") ) {
						l = new serialListener<viconProcessor::VICONPacket_t,PARSER_T>(_address, _port, dynamic_cast<processor*>(_processor));
					} else if( !_name.compare("odo") ) {
						l = new serialListener<odoProcessor::ODOPacket_t,PARSER_T>(_address, _port, dynamic_cast<processor*>(_processor));
					} else if( !_name.compare("imu") ) {
						l = new serialListener<imuProcessor::IMUPacket_t,PARSER_T>(_address, _port, dynamic_cast<processor*>(_processor));
					}
				} else if( !_type.compare("pipe") ) {
					l = new pipeListener<PARSER_T>(_address, _port, dynamic_cast<PARSER_T*>(_processor));
				} else if( !_type.compare("terminal") ) {
					l = new terminalListener<PARSER_T>(dynamic_cast<processor*>(_processor));
				}
			}
			catch  (std::exception& e) {
				std::cerr << e.what() << std::endl;
			}
			
			return l;
		}
		
		
		/** \fn listener* getListener( const std::string& _type, const std::string& _address, short _port, const std::string& _name, processor* _processor, filter* _filter )
		 *  \brief Returns an initialized listener
		 *  \ingroup Communication
		 *
		 *  \details Given, depending on the listener type, an address/port/name, a port number/baud rate, and the name of the vehicle, the function will return an initialized listener.
		 *
		 *  \param _type Type of communication (udp, serial etc.)
		 *  \param _address Address/port/name of the communicator
		 *  \param _port Port/Baudrate etc of the communicator
		 *  \param _name Name of the vehicle
		 *  \param _processor An initialized processor
		 *  \param _filter An initialized processor
		 *  \return An initialized listener
		 */
		template <class PARSER_T>
		listener<PARSER_T>* getCommanderListener(const std::string& _type, // type of communication
												 const std::string& _address, int _port,
												 PARSER_T* _processor) {
			
			listener<PARSER_T>* l = NULL;
			
			try {
				if( !_type.compare("udp") ) {
					l = new udpListener<PARSER_T>(_address, _port, dynamic_cast<PARSER_T*>(_processor));
				} else if( !_type.compare("serial") ) {
					//	l = new serialListener<char>(_address, _port, dynamic_cast<processor*>(_processor));
				}  else if( !_type.compare("terminal") ) {
					l = new terminalListener<PARSER_T>(dynamic_cast<PARSER_T*>(_processor));
				}
			}
			catch  (std::exception& e) {
				std::cerr << e.what() << std::endl;
			}
			
			return l;
		}
		
		
		/** \fn listener* getDuplex( const std::string& _type, const std::string& _address, short _port, const std::string& _name, processor* _processor, filter* _filter )
		 *  \brief Returns an initialized listener
		 *  \ingroup Communication
		 *
		 *  \details Given, depending on the listener type, an address/port/name, a port number/baud rate, and the name of the vehicle, the function will return an initialized listener.
		 *
		 *  \param _type Type of communication (udp, serial etc.)
		 *  \param _address Address/port/name of the communicator
		 *  \param _port Port/Baudrate etc of the communicator
		 *  \param _name Name of the vehicle
		 *  \param _processor An initialized processor
		 *  \param _filter An initialized processor
		 *  \return An initialized listener
		 */
		template <class PARSER_T>
		communicator* getDuplex(const std::string& _type, // type of communication
								const std::string& _address, int _port,
								const std::string& _name, // name of sensor
								PARSER_T* _processor) {
			
			listener<PARSER_T>* l = NULL;
			
			try {
				if( !_type.compare("udp") ) {
				} else if( !_type.compare("serial") ) {
					if( !_name.compare("ptam") ) {
						std::cout << "getting serial ptam\n";
						l = new serialDuplex<ptamProcessor::PTAMPacket_t,PARSER_T>(_address, _port, _processor);
					} else if( !_name.compare("odo") ) {
						l = new serialDuplex<odoProcessor::ODOPacket_t,PARSER_T>(_address, _port, _processor);
					} else if( !_name.compare("vicon") ) {
						l = new serialDuplex<viconProcessor::VICONPacket_t,PARSER_T>(_address, _port, _processor);
					}
				}
			}
			catch  (std::exception& e) {
				std::cerr << "Error getDuplex() " << e.what() << std::endl;
			}
			
			return l;
		}
		
		
		/** \fn template <class T> boost::shared_ptr<T> newTransmitter( const std::string& _address, int _port )
		 *  \brief Returns an initialized transmitter
		 *  \ingroup Communication
		 *
		 *  \details Given, depending on the listener type, an address/port/name, a port number/baud rate, the function will return an initialized transmitter.
		 *
		 *  \tparam L The listener class
		 *
		 *  \param _address Address/port/name of the communicator
		 *  \param _port Port/Baudrate etc of the communicator
		 *  \return An initialized transmitter
		 */
		template <class T>
		boost::shared_ptr<T> newTransmitter( const std::string& _address, int _port ) {
			
			try {
				boost::shared_ptr<T> t( new T(_address, _port) );
				return t;
			}
			catch  (std::exception& e) {
				std::cerr << e.what() << std::endl;
			}
			
			return boost::shared_ptr<T>();
		}
		
	} // namespace comm
} // namespace sefu

#endif // _COMMUNICATIONS_H_
