#ifndef _COMMANDRECEIVERS_H_
#define _COMMANDRECEIVERS_H_

#include "command_receivers/command_receiver.h"

#include <string>

namespace sefu { namespace cmrc {
	
	inline commandReceiver* getCommandReceiver( const std::string& _type ) {
		commandReceiver* cr = NULL;
		
        if( !_type.compare("ptam") ) {
            cr = new ptamCommandReceiver();
        }
		
		
		if( !cr ) {
			cr = new commandReceiver();
		}
		
		return cr;
	}

	
	
} // namespace cmrc
} // namespace sefu


#endif // _COMMANDRECEIVERS_H_