/** \file filter.h
 *  \brief Header containing base classes for filters
 *  \author Niklas Bergström
 *
 *  \details There are two main classes for communication: listener
 *  and transmitter. They contain the structure needed for subclasses
 *  that wish to implement communication protocols such as serial
 *  or udp.
 */
#ifndef _FILTER_H_
#define _FILTER_H_

/** namespace sefu */
namespace sefu {
    /** namespace sefu::filt */
    namespace filt {
        
        /** \class filter filter.h "filter.h"
         *  \brief Base class for filtering raw data
         *  \author Niklas Bergström
         *  \version 0.1
         *  \ingroup Filters
         *
         *  \details The filter class is an abstract class for filtering raw data.
         *  Subclasses should implement the given methods which will enable filtering
         *  the raw incoming sensor data. Each subclass should internally hold some
         *  structure and be able to return the filtered data on command.
         */
        class filter{
        public:
            /** \brief Get the filtered data
             *
             *  \return SensorStruct with the filtered data
             */
            virtual const SensorStruct& getFilteredData() const = 0;
            
            
            /** \brief Filter data
             *
             *  \details This is where the action is. Each time a full packet is received
             *  from the sensor, this method is called.
             *
             *  \param _data A filled SensorStruct
             */
            virtual bool filterData(const SensorStruct& _data) = 0;
            
            
            /** \brief Virtual destructor */
            virtual ~filter() {}
			
			float scale() const { return mScale; }
			
			void setScale(float _s) { mScale = _s; }
			
		protected:
			float mScale;
        };
        
    } // namespace filt
} // namespace sefu

#endif // _FILTER_H_
