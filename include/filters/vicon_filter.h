/** \file vicon_filter.h
 *  \brief Header containing the viconFilter class
 *  \author Niklas Bergström
 *
 *  \details The viconFilter implments the filter for vicon data
 *  as done in the vehicle wrapper.
 *
 */
#ifndef _VICONFILTER_H_
#define _VICONFILTER_H_

#include "filters/filter.h"

/** namespace sefu */
namespace sefu {
	/** namespace sefu::filt */
	namespace filt {
		
		/** class viconFilter vicon_filter.h "vicon_filter.h"
		 *  \brief Filter for vicon data
		 *  \author Niklas Bergström
		 *  \ingroup Filters
		 *
		 *  \details Implments the Vicon filter from the vehicle wrapper
		 *  software.
		 *  \todo Implment the filter
		 */
		class viconFilter : public filter {
			
			// Documented in superclass
			virtual const SensorStruct& getFilteredData() const {
				return mSensorData;
			}
			
			
			// Documented in superclass
			virtual bool filterData(const SensorStruct& _data) {
				sOrientation o = _data.orientation;
				float thetahalf = o[0]*o[2] - o[1]*o[3];
				float phihalf = o[0]*o[1]+o[2]*o[3];
				
				
				// Problems that can occur here is that the vehicle is:
				//	 (1) discovered in a wrong position
				//   (2) discovered in the right position but with inaccurate pose
				
				
				mSensorData = _data;
				
				/*				if( fabs(thetahalf) > 0.06f || fabs(phihalf) > 0.06f) {
					std::cout << "vicon out of plane\n";
					mSensorData.quality = 0;
					return false;
					}*/
				
				if( fabs(_data.position[0]) > 4.8f || fabs(_data.position[1]) > 7.0) {
					std::cout << "vicon out of bounds\n";
					mSensorData.quality = 0;
					return true;
				}
				
				if( _data.quality == 0 ) {
				  std::cout << "vicon quality bad\n";
				  mSensorData.quality = 0;
				  return true;
				}
				
				//std::cout << "vicon good\n";
				
				return true;
			}
			
		private:
			SensorStruct mSensorData;
		};
		
	} // namespace filt
} // namespace sefu

#endif // _VICONFILTER_H_
