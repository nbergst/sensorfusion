/** \file vicon_filter.h
 *  \brief Header containing the viconFilter class
 *  \author Niklas Bergström
 *
 *  \details The viconFilter implments the filter for vicon data
 *  as done in the vehicle wrapper.
 *
 */
#ifndef _PTAMFILTER_H_
#define _PTAMFILTER_H_

#include "filters/filter.h"

#include "coordinate_transform.h"

/** namespace sefu */
namespace sefu {
	/** namespace sefu::filt */
	namespace filt {
		
		/** class viconFilter vicon_filter.h "vicon_filter.h"
		 *  \brief Filter for vicon data
		 *  \author Niklas Bergström
		 *  \ingroup Filters
		 *
		 *  \details Implments the Vicon filter from the vehicle wrapper
		 *  software.
		 *  \todo Implment the filter
		 */
		class ptamFilter : public filter {
			
			// Documented in superclass
			virtual const SensorStruct& getFilteredData() const {
				return mSensorData;
			}
			
			
			// Documented in superclass
			virtual bool filterData(const SensorStruct& _data) {
				static bool resetting = 0;
				
				static int resetcounter = 0;
				
				/*if( resetting && _data.quality > 0 ) {
				 std::cout << "waiting for ptam to reset\n";
				 mSensorData.quality = 0;
				 resetcounter += 1;
				 if( resetcounter == 100 ) {
				 resetcounter = 0;
				 return false;
				 }
				 return true;
				 }*/
				resetting = false;
				
				// We have no data or no scale, so we want to fill that in
				if( mSensorData.quality < 1 ) {
					//				  std::cout << _data.quality << " data quality\n";
					// New data is accurate, use it! Else, do nothing
					resetcounter += 1;
					if( _data.quality > 0 ) {
						std::cout << "Got new good data\n";
						mSensorData = _data;
						return true;
					}
					if( resetcounter == 100 ) {
						resetcounter = 0;
						return false;
					}
					//std::cout << "waiting for initialization\n";
					return true;
				} else {
					Quaternion ori;
					ori[0] = _data.orientation[0];
					ori[1] = _data.orientation[1];
					ori[2] = _data.orientation[2];
					ori[3] = _data.orientation[3];
					
					
					// YouBot specific
					Quaternion transform(0.249,-0.9703,0.0,0.0);
					
					Quaternion o = ori*transform;
					
					float thetahalf = o[0]*o[2] - o[1]*o[3];
					float phihalf = o[0]*o[1]+o[2]*o[3];
					
					//				std::cout << "ptam quaternion: " << o << std::endl;
					//std::cout << "ptam roll/pitch: " << thetahalf << " " << phihalf << std::endl;
										
					//					bool outOfPlane = fabs(thetahalf) > 0.08f || fabs(phihalf) > 0.08f;
					//					std::cout << "thetahalf: " << thetahalf << " phihalf: " << phihalf << std::endl;
					//					if( outOfPlane ) {
					//				  std::cout << "ptam out of plane: " << thetahalf << " " << phihalf << std::endl;
					//					}
					
					// Incoming data is no good, don't use it
					if( _data.quality < 1 ) {
					  mSensorData.quality = 0;
					  resetcounter = 0;
					  return false;
					}
					// PTAM thinks incoming data is good. Let's check and see if we agree
					/*					if( mSensorData.position.norm(_data.position) > 0.2f ||
					    fabs(mSensorData.orientation.norm(_data.orientation)) > 0.5f ) {
					  std::cout << "Scale: " << filter::mScale << std::endl;
					  std::cout << "PTAM movement: " << filter::mScale*mSensorData.position.norm(_data.position) << std::endl;
					  std::cout << "PTAM angular movement: " << fabs(mSensorData.orientation.norm(_data.orientation)) << std::endl;
					  mSensorData.quality = 0;
					  resetting = true;
					  resetcounter = 0;
					  //return false;
					  } else {*/
					  mSensorData = _data;
					  //					}
				}
				return true;
			}
			
		private:
			double mScale;
			SensorStruct mSensorData;
		};
		
	} // namespace filt
} // namespace sefu

#endif // _PTAMFILTER_H_
