/** \file null_filter.h
 *  \brief Header containing the nullFilter class
 *  \author Niklas Bergström
 *
 */
#ifndef _NULLFILTER_H_
#define _NULLFILTER_H_

#include "filters/filter.h"

/** namespace sefu */
namespace sefu {
    /** namespace sefu::filt */
    namespace filt {
        
    /** \class nullFilter null_filter.h "null_filter.h"
     *  \brief Simpel pass-through filter
     *  \author Niklas Bergström
     *  \version 0.1
     *  \ingroup Filters
     *
     *  \details This filter is not doing any filtering, but
     *  rather just passes the data through.
     */
    class nullFilter : public filter {
        
        /** \brief Getting filtered data */
        virtual const SensorStruct& getFilteredData() const {
            return mSensorData;
        };
        
        /** \brief Filtering incoming data
         *
         *  \details The null filter simply stores the incoming data
         *  for later use in the sensor fusion
         */
        virtual bool filterData(const SensorStruct& _data) {
		  mSensorData = _data;
		  return true;
        }
        
    private:
        SensorStruct mSensorData;
    };
    
    } // namespace filt
} // namespace sefu

#endif // _NULLFILTER_H_
