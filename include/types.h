/** \file types.h
 *  \brief Header containing data types used throughout the program
 *  \author Niklas Bergström
 *
 *  \details
 */
#ifndef _TYPES_H_
#define _TYPES_H_

#include <sstream>

#include "coordinate_transform.h"

/** \defgroup Types Sensor Fusion Data types
 *  This group contains unions, structs etc used in the program
 */

/** namespace sefu */
namespace sefu {
    
    /** \union sPosition
     *  \brief Union representing a 3D position
     *  \author Niklas Bergström
     *  \ingroup Types
     */
    struct sPosition {
		float p[3];
		
		float norm( const sPosition& _p2 ) {
			return sqrt((this->p[0]-_p2[0])*(this->p[0]-_p2[0]) +
						(this->p[1]-_p2[1])*(this->p[1]-_p2[1]) +
						(this->p[2]-_p2[2])*(this->p[2]-_p2[2]));
		}
		
	  sPosition& operator=(const sPosition& _p)  {
		this->p[0] = _p[0];
		this->p[1] = _p[1];
		this->p[2] = _p[2];
		return *this;
	  }

		float& operator[](int _idx) {
			switch( _idx ) {
				case 0:
					return this->p[0];
					break;
				case 1:
					return this->p[1];
					break;
				case 2:
					return this->p[2];
					break;
				default:
					std::cout << "Trying to access quaternion out of bounds\n";
					exit(-1);
					break;
			}
			// To supress warning
			return this->p[0];
		}
		
		float operator[](int _idx) const {
			switch( _idx ) {
				case 0:
					return this->p[0];
					break;
				case 1:
					return this->p[1];
					break;
				case 2:
					return this->p[2];
					break;
				default:
					std::cout << "Trying to access quaternion out of bounds\n";
					exit(-1);
					break;
			}
			// To supress warning
			return 0.0f;
		}

	  
		
    } __attribute__((packed));
	
	
    /** \union sOrientation
     *  \brief Struct representing a quaternion
     *  \author Niklas Bergström
     *  \ingroup Types
     */
    struct sOrientation {
		float p[4];

		float norm( const sOrientation& _o ) {
			return sqrt((this->p[0]-_o.p[0])*(this->p[0]-_o.p[0]) +
						(this->p[1]-_o.p[1])*(this->p[1]-_o.p[1]) +
						(this->p[2]-_o.p[2])*(this->p[2]-_o.p[2]) +
						(this->p[3]-_o.p[3])*(this->p[3]-_o.p[3]));
		}
		
		float& operator[](int _idx) {
			switch( _idx ) {
				case 0:
					return this->p[0];
					break;
				case 1:
					return this->p[1];
					break;
				case 2:
					return this->p[2];
					break;
				case 3:
					return this->p[3];
					break;
				default:
					std::cout << "Trying to access quaternion out of bounds\n";
					exit(-1);
					break;
			}
			// To supress warning
			return this->p[0];
		}

		float operator[](int _idx) const {
			switch( _idx ) {
				case 0:
					return this->p[0];
					break;
				case 1:
					return this->p[1];
					break;
				case 2:
					return this->p[2];
					break;
				case 3:
					return this->p[3];
					break;
				default:
					std::cout << "Trying to access quaternion out of bounds\n";
					exit(-1);
					break;
			}
			// To supress warning
			return 0.0f;
		}
	} __attribute__((packed));
    
    
    /** \struct VehiclePacket_t
     *  \brief Struct for sending to the vehicle
     *  \author Niklas Bergström
     *  \ingroup Types
     */
    struct VehiclePacket_t {
        double timestamp;  /** \var timestamp Timestamp */
        char name[16];   /** \var name The name of the sensor */
        short id; /** \var id The id as given by the sensor */
        short quality; /** \var quality Quality as indicated by the sensor */
        float position[3]; /** \var quality Position as estimated by the sensor */
        float orientation[4]; /** \var quality Orientation as estimated by the sensor */
        
        std::string encodeToString() const {
            std::stringstream ss;
            ss << timestamp << ";" << name << " " << id << "," << quality << ", " <<
            position[0] << "," << position[1] << "," << position[2] << "," <<
            orientation[0] << "," << orientation[1] << "," <<
            orientation[2] << "," << orientation[3] << ";\0";
            
            return ss.str();
        }
        
    } __attribute__((packed));
    
    
    
    /** \struct LoggingPacket_t
     *  \brief Struct for sending to the logger
     *  \author Niklas Bergström
     *  \ingroup Types
     */
    struct LoggingPacket_t {
        size_t timestamp; /** \var timestamp Timestamp */
        char name[16];  /** \var name The name of the sensor */
        short quality; /** \var quality Quality of sensor data */
        sPosition position; /** \var position Estimated position */
        sOrientation orientation; /** \var orientation Estimated orientation */
        sPosition positionVar; /** \var positionVar Estimated position variance */
        sOrientation orientationVar; /** \var orientationVar Estimated orientation variance */
        
        /** \brief Encodes the logging packet to a string
         *
         *  \return A string representing the packet
         */
        std::string encodeToString() const {
            std::stringstream ss;
            ss << timestamp << ";" << quality << ", " <<
            position[0] << "," << position[1] << "," << position[2] << "," <<
            orientation[0] << "," << orientation[1] << "," <<
            orientation[2] << "," << orientation[3] << ";\0";
            
            return ss.str();
        }
        
    } __attribute__((packed));
    
    
    
    
    enum eQualityLevel {
        SENSOR_NO_DATA = 0,
        SENSOR_HAS_DATA,
    };
	
	
	
    /** \struct SensorStruct
     *  \brief Struct for sending to the logger
     *  \author Niklas Bergström
     *  \ingroup Types
     */
    struct SensorStruct {
        // Timestamp
        size_t timestamp;
        // Id as referenced by the sensor
        short id;
        // Quality of the sensor data
        short quality;
        // Estimated position
        sPosition position;
        // Estimated orientation
        sOrientation orientation;
        // Estimated variance of position
        sPosition positionVar;
        // Estimated variance of orientation
        float orientationVar;
		
        SensorStruct() {
            timestamp = 0;
            id = 0;
            quality = 0;
            position[0] = position[1] = position[2] = 0.0f;
            positionVar[0] = positionVar[1] = positionVar[2] = 0.0f;
//            positionOffset.pos[0] = positionOffset.pos[1] = positionOffset.pos[2] = 0.0f;
            orientation[0] = 1.0f;
            orientation[1] = orientation[2] = orientation[3] = 0.0f;
            orientationVar = 0.0f;//.ori[0] = orientationVar.ori[1] = orientationVar.ori[2] = orientationVar.ori[3] = 0.0f;
//            orientationOffset.ori[0] = 1.0f;
//            orientationOffset.ori[1] = orientationOffset.ori[2] = orientationOffset.ori[3] = 0.0f;
//            scaleOffset = 1.0f;
        }
        
        std::string encodeToString() const {
            std::stringstream ss;
            ss << timestamp << ";" << " " << id << "," << quality << ", " <<
            position[0] << "," << position[1] << "," << position[2] << "," <<
            orientation[0] << "," << orientation[1] << "," <<
            orientation[2] << "," << orientation[3] << ";\0";
            
            return ss.str();
        }

    } __attribute__((packed));

    
} // namespace sefu

#endif // _TYPES_H_
