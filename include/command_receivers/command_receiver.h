#ifndef _COMMANDRECEIVER_H_
#define _COMMANDRECEIVER_H_


namespace sefu {
	
	class sensor;
	
	namespace cmrc {
		
		class commandReceiver {
		public:
			virtual void processCommand(char _c, sensor* _s) {}
			
			virtual ~commandReceiver() {}
		};
		
		
		class ptamCommandReceiver : public commandReceiver {
		public:
			virtual void processCommand(char _c, sensor* _s);
			
			virtual ~ptamCommandReceiver() {}
		};
		
	} // namespace cmrc
} // namespace sefu

#endif // _COMMANDRECEIVER_H_