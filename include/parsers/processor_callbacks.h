/** \file processor_callbacks.h
 *  \brief Processor callback related classes
 *  \author Niklas Bergström
 *
 *  \details Base class for processor callbacks, and a set of
 *  implemented callbacks.
 */
#ifndef _PROCESSORCALLBACKS_H_
#define _PROCESSORCALLBACKS_H_

#include <iostream>

#include <functional>

/** namespace sefu */
namespace sefu {
    /** namespace sefu::proc */
    namespace pars {
		
		template <class T>
		using callback = std::function<void (T)>;

		
        class processor;
        
        /** \class processorCallback processor_callbacks.h "processor_callbacks.h"
         *  \brief Callbacks for the processor to call when new data arrives
         *  \author Niklas Bergström
         *  \ingroup Processors
         *
         *  \details Subclasses of the processorCallback class can be added to a listener
         *  and will be called each time a whole data packet has arrived.
         */
        class processorCallback {
            
        public:
            /** \brief Operator that will be called after arrival of data in the listener */
            virtual void operator()(boost::shared_ptr<processor> _processor) {
                
            }
            
        protected:
            /** \brief Constructor. Protected to avoid direct subclassing */
            processorCallback() {}
            
        };
        
        
        /** \class coutCallback processor_callbacks.h "processor_callbacks.h"
         *  \brief Simply echoes the incoming message to cout
         *  \author Niklas Bergström
         *  \ingroup Processors
         */
        class coutCallback : public processorCallback {
        public:
            /** \brief Callback function echoing the incoming packet to cout
             *
             *  \param _processor The processor holding the incoming sensor data
             *
             *  \todo Implement the echoing
             */
            virtual void operator()(boost::shared_ptr<processor> _processor) {
                //std::cout << "Message: " << _processor-> << std::endl;
            }
        };
        
        
        /** \class echoCallback processor_callbacks.h "processor_callbacks.h"
         *  \brief Echoes the incoming message through a transmitter
         *  \author Niklas Bergström
         *  \ingroup Processors
         *
         *  \tparam A subclass of \c transmitter
         *
         *  \todo Decide what processor method to return
         */
        template <class T>
        class echoCallback : public processorCallback {
        private:
            T mTransmitter;
            
        public:
            /** \brief Constructor
             *
             *  \details Templated with a transmitter which will be used for the echoing
             *  \param _t The transmitter which to use for the sending
             */
            echoCallback( const T& _t ) {
                mTransmitter = _t;
            }
            
            
            /** \brief Callback function echoing the incoming packet to cout 
             *
             *  \param _processor The processor holding the incoming sensor data
             */
            virtual void operator()(boost::shared_ptr<processor> _processor){
                //mTransmitter.sen
            }
            
        };
        
    } // namespace pars
} // namespace sefu

#endif // _PROCESSORCALLBACKS_H_
