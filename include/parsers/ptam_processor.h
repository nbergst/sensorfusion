/** \file ptam_processor.h
 *  \brief Header containing class for processing incoming PTAM data
 *  \author Niklas Bergström
 *
 */
#ifndef _PTAMPROCESSOR_H_
#define _PTAMPROCESSOR_H_

#include "parsers/processor.h"

/** namespace sefu */
namespace sefu {
	/** namespace sefu::proc */
	namespace pars {
		
		/** \class ptamProcessor ptam_processor.h "ptam_processor.h"
		 *  \brief Processes incoming PTAM data
		 *  \author Niklas Bergström
		 *  \ingroup Processors
		 *
		 *  \details Handles incoming packets from the PTAM sensor, including making
		 *  sure that the whole packet has arrived and parsing the packet.
		 *
		 *  \todo Currently the class is specific to serial communication. The serial
		 *  handshaking should be moved to \c serialCommunication
		 */
		class ptamProcessor : public processor {
			
		public:
			/** \struct PTAMPacket_t
			 *  \brief Struct representing the incoming data from PTAM
			 */
			struct PTAMPacket_t {
				uint8_t Header;
				uint64_t Time; // In microseconds since epoch
				float Position[3];
				float Orientation[4];
				uint8_t HasTracking;
				uint16_t Checksum;
			} __attribute__((packed));
			
			
		public:
			/** \brief Constructor
			 *
			 *  \details Takes the vehicle name as a parameter which could be used
			 *  for identification if several sensors are using the same \c listener port
			 *  \param _vehicleName The name of the vehicle
			 */
			ptamProcessor(std::string _vehicleName) : processor(_vehicleName) {
				mPacket.HasTracking = 0;
			}
			
			
			/** \brief Virtual destructor */
			virtual ~ptamProcessor() {}
			
			
			/** \brief Parses an incoming packet
			 *
			 *  \details Takes a char buffer and the size of the buffer and parses
			 *  the packet if one full buffer has arrived.
			 *  \param _message The incoming packet
			 *  \param _nbytes The number of incoming bytes
			 *  \return True if one message was successfully parse. False otherwise
			 *
			 *  \todo Move assumption of serial communication to the \c serialCommunication class
			 */
			virtual bool parse( const char* _message, size_t _nbytes ) {
				
				mPacket = *((PTAMPacket_t*)_message);
				
				mRawSensorData.timestamp = mPacket.Time;
				mRawSensorData.position[0] = mPacket.Position[0];
				mRawSensorData.position[1] = mPacket.Position[1];
				mRawSensorData.position[2] = mPacket.Position[2];
				
				sOrientation tori;
				tori[0] = mPacket.Orientation[0];
				tori[1] = mPacket.Orientation[1];
				tori[2] = mPacket.Orientation[2];
				tori[3] = mPacket.Orientation[3];
				
				processor::standardQuaternion(tori);
				
				mRawSensorData.orientation = tori;
				
				if( mRawSensorData.quality ) {
					processor::mLastGoodData = mRawSensorData;
				}
				
				/*mRawSensorData.orientation[0] = mPacket.Orientation[0];
				 mRawSensorData.orientation[1] = mPacket.Orientation[1];
				 mRawSensorData.orientation[2] = mPacket.Orientation[2];
				 mRawSensorData.orientation[3] = mPacket.Orientation[3];*/
				
				//				if( mPacket.HasTracking & 0x02 ) {
				//	mRawSensorData.quality = 2;
				//} else 
				if( mPacket.HasTracking & 0x01 ) {
					mRawSensorData.quality = 1;
				} else {
					mRawSensorData.quality = 0;
				}

				if( mPacket.HasTracking & 0x02 ) {
				  mRawSensorData.quality += 2;
				}
				
				// Message parsed, so reset
				mNew = true;
				
				return true;
			}
			
			
			/** \brief Checks for tracking
			 *
			 *  \return True if PTAM has reported good tracking
			 */
			virtual bool hasTracking() {
				return mPacket.HasTracking & 0x1;
			}
			
			
			/** \brief Checks for scale
			 *
			 *  \return True if PTAM has reported it has scale
			 */
			virtual bool hasScale() {
				return mPacket.HasTracking & 0x2;
			}
			
			
		private:
			PTAMPacket_t mPacket;
			
		};
		
	} // namespace pars
} // namespace sefu

#endif // _PTAMPROCESSOR_H_
