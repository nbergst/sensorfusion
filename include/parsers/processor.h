/** \file processor.h
 *  \brief Holding processor base class
 *  \author Niklas Bergström
 *
 *  \details
 */
#ifndef _PROCESSOR_H_
#define _PROCESSOR_H_

#include <boost/shared_ptr.hpp>

#include "types.h"

/** namespace sefu */
namespace sefu {
	
	class parser {
	public:
		virtual ~parser() {}
	};
	
	/** namespace sefu::proc */
	namespace pars {
		
		/** \class processor processor.h "processor.h"
		 *  \brief Base class for processors
		 *  \author Niklas Bergström
		 *  \ingroup Processors
		 *
		 *  A processor for a certain sensor needs to inherit from this class. The important
		 *  function is parse, which takes a buffer and a byte count, and which should fill the SensorStruct
		 *  with the received data.
		 */
		class processor : public sefu::parser {
		public:
			/** \brief Parses the incoming data
			 *
			 *  \details This method needs to be provided by all processors.
			 *
			 *  \param _message Incoming buffer
			 *  \param _nBytes Size of the incoming buffer
			 *  \return Whether the data was successfully parsed
			 *
			 *  \remarks A false return value might indicate that not all data has
			 *  been received yet
			 */
			virtual bool parse( const char* _message, size_t _nBytes ) { return false; }
			
			
			virtual bool hasTracking() { return true; }
			
			
			virtual bool hasScale() { return true; }
			
			
			virtual size_t timestamp() { return mRawSensorData.timestamp; }
			
			
			virtual short quality() { return mRawSensorData.quality; }
			
			
			void standardQuaternion(sOrientation& _q) {
				
				const sOrientation& qold = processor::mLastGoodData.orientation;
				
				int idx = 0;
				for( int i=1; i<4; ++i ) {
					if( fabs(qold[i]) > fabs(qold[idx]) ) {
						idx = i;
					}
				}
				

				if( _q[idx]*qold[idx] < 0 ) {
					_q[0] = -_q[0];
					_q[1] = -_q[1];
					_q[2] = -_q[2];
					_q[3] = -_q[3];
				}
			}
			
			
			/** \brief Getter for the vehicle id
			 *
			 *  \details Each sensor might assign a unique id to the vehicle.
			 *  The processor should pick up this for the \c sensor to use
			 */
			virtual short id() const { return mRawSensorData.id; }
			
			
			/** \brief Getter for the current position estimate
			 *
			 *  \return A constant reference to the current position estimate
			 */
			virtual const sPosition& position() const { return mRawSensorData.position; }
			
			
			/** \brief Getter for the current orientation estimate
			 *
			 *  \return A constant reference to the current orientation estimate
			 */
			virtual const sOrientation& orientation() const { return mRawSensorData.orientation; }
			
			
			/** \brief Getter for the raw sensor data
			 *
			 *  \returns A constant reference to the raw sensor data
			 */
			const SensorStruct& rawData() const { return mRawSensorData; }
			
			
			/** \brief Virtual destructor */
			virtual ~processor() {}
			
			
			/** \var SensorStruct mLastGoodData
			 *
			 */
			SensorStruct mLastGoodData;
			
			
		protected:
			/** \brief Constructor
			 *
			 *  \details Only function of the constructor is to assign the
			 *  vehicle name to the base class. Protected since only subclasses
			 *  should be instantiated.
			 *
			 *  \param _vehicleName The name of the vehicle
			 */
			processor( std::string _vehicleName ) : mVehicleName(_vehicleName) {}
			
			
			/** \var const std::string mVehicleName
			 *  \brief The vehicle name
			 *
			 *  The vehicle name is needed in case a sensor (like vicon) is sending
			 *  information for multiple vehicles at the same time. The vehicle name
			 *  is then used to identify which data pertains to this processor
			 */
			const std::string mVehicleName;
			
			
			/** \var bool mValid
			 *  \brief Indicates if the current estimate is valid
			 */
			bool mValid;
			
			
			/** \var bool mNew
			 *  \brief Is set to true whenever new data arrives.
			 *
			 *  \details In order for other classes using the processor to know
			 *  when new data has arrived, this should be set to true by the
			 *  processor once the incoming data has been processed.
			 */
			bool mNew;
			
			
			/** \var SensorStruct mRawSensorData
			 *  \brief Represents the current belief of the raw sensor data
			 *
			 *  \details It is the task of the processor to populate this struct
			 *  with new values as soon as new data has arrived
			 */
			SensorStruct mRawSensorData;
			
			
		};
		
	} // namespace pars
} // namespace sefu

#endif // _PROCESSOR_H_
