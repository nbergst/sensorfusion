/** \file vicon_processor.h
 *  \brief Header containing class for processing incoming Vicon data
 *  \author Niklas Bergström
 *
 */
#ifndef _VICONPROCESSOR_H_
#define _VICONPROCESSOR_H_

#include "parsers/processor.h"

/** namespace sefu */
namespace sefu {
	/** namespace sefu::proc */
	namespace pars {
		
		/** \class viconProcessor vicon_processor.h "vicon_processor.h"
		 *  \brief Processes incoming Vicon data
		 *  \author Niklas Bergström
		 *  \ingroup Processors
		 *
		 *  \details Handles incoming packets from the Vicon sensor including parsing the packet.
		 */
		class viconProcessor : public processor {
			
		public:
			/** \struct VICONPacket_t
			 *  \brief Struct representing the incoming data from Vicon
			 */
			struct VICONPacket_t {
				uint8_t Header;
				double timestamp;
				char name[100];
				short id;
				short quality;
				float x,y,z;
				float q0,q1,q2,q3;
				uint16_t Checksum;
			}  __attribute__((packed));
			
			
			/** \brief Constructor
			 *
			 *  \details Takes the vehicle name as a parameter which could be used
			 *  for identification if several sensors are using the same \c listener port
			 *  \param _vehicleName The name of the vehicle
			 */
			viconProcessor(std::string _vehicleName) : processor(_vehicleName) {}
			
			
			virtual void setTimestamp( size_t &_timestamp ) {
				_timestamp = mPacket.timestamp;
			}
			
			virtual void setQuality( short& _quality ) {
				_quality = mPacket.quality;
			}
			
			
			virtual void setName( std::string& _name ) {
				_name = mPacket.name;
			}
			
			virtual void setId( short& _id ) {
				_id = mPacket.id;
			}
			
			
			virtual short quality() {
			  return mRawSensorData.quality;
			}

			/** \brief Parses an incoming packet
			 *
			 *  \details Takes a char buffer and the size of the buffer and parses
			 *  the packet if one full buffer has arrived. It is assumed that the data
			 *  for several vehicles can arrive in the same packet. The parsing checks
			 *  for the vehicle name and parses the data with the same name as specified
			 *  in the config file
			 *  \param _message The incoming packet
			 *  \param _nbytes The number of incoming bytes
			 *  \return True if one message was successfully parse. False otherwise
			 *
			 *  \todo Move assumption of serial communication to the \c serialCommunication class
			 */
			bool parse( const char* _message, size_t _nbytes ) {
				
				const std::string m(_message);
				
				// Timestamp
				size_t idx0 = 0;
				size_t idx1 = m.find(';');
				mPacket.timestamp = std::atof(m.substr(idx0,idx1).c_str());
				
				const char* tmessage = _message + idx1+1;
				
				float  tx, ty, tz, tqw, tqx, tqy, tqz;
				int    veh_id;
				int    data_quality;

				while( sscanf(tmessage, "%s%d,%d,%f,%f,%f,%f,%f,%f,%f;", mPacket.name, &veh_id, &data_quality, &tx, &ty, &tz, &tqw, &tqx, &tqy, &tqz ) == 10 ) {
					
				  // std::cout << m << std::endl;

					// Make sure we're parsing the correct vehicle
				  //std::cout << mVehicleName << " " << mPacket.name << std::endl;
					if(!mVehicleName.compare(mPacket.name)) {
						mPacket.id = veh_id;
						mPacket.quality = data_quality;
						mPacket.x = tx;
						mPacket.y = ty;
						mPacket.z = tz;
						mPacket.q0 = tqw;
						mPacket.q1 = tqx;
						mPacket.q2 = tqy;
						mPacket.q3 = tqz;
						break;
					}
					
					idx1 = m.find(';',idx1+1);
					tmessage = _message + idx1+1;
				}
				
				mRawSensorData.id = mPacket.id;
				mRawSensorData.timestamp = mPacket.timestamp;
				mRawSensorData.quality = mPacket.quality;
				mRawSensorData.position[0] = mPacket.x;
				mRawSensorData.position[1] = mPacket.y;
				mRawSensorData.position[2] = mPacket.z;

				sOrientation tori;
				tori[0] = mPacket.q0;
				tori[1] = mPacket.q1;
				tori[2] = mPacket.q2;
				tori[3] = mPacket.q3;

				processor::standardQuaternion(tori);
				
				mRawSensorData.orientation = tori;

				if( mRawSensorData.quality ) {				  
				  processor::mLastGoodData = mRawSensorData;
				}

				/*				mRawSensorData.orientation[0] = mPacket.q0;
				mRawSensorData.orientation[1] = mPacket.q1;
				mRawSensorData.orientation[2] = mPacket.q2;
				mRawSensorData.orientation[3] = mPacket.q3;*/


				return true;
			}
			
			
			virtual void setPosition( float _position[3] ) {
				_position[0] = mPacket.x;
				_position[1] = mPacket.y;
				_position[2] = mPacket.z;
			}
			
			virtual void setOrientation( float _orientation[4] ) {
				_orientation[0] = mPacket.q0;
				_orientation[1] = mPacket.q1;
				_orientation[2] = mPacket.q2;
				_orientation[3] = mPacket.q3;
			}
			
			
		private:
			VICONPacket_t mPacket;
			
		};
		
	} // namespace pars
} // namespace sefu

#endif // _VICONPROCESSOR_H_
