/** \file imu_processor.h
 *  \brief Header containing class for processing incoming PTAM data
 *  \author Niklas Bergström
 *
 */
#ifndef _IMUPROCESSOR_H_
#define _IMUPROCESSOR_H_

#include "parsers/processor.h"

/** namespace sefu */
namespace sefu {
	/** namespace sefu::proc */
	namespace pars {
		
		/** \class ptamProcessor ptam_processor.h "ptam_processor.h"
		 *  \brief Processes incoming PTAM data
		 *  \author Niklas Bergström
		 *  \ingroup Processors
		 *
		 *  \details Handles incoming packets from the PTAM sensor, including making
		 *  sure that the whole packet has arrived and parsing the packet.
		 *
		 *  \todo Currently the class is specific to serial communication. The serial
		 *  handshaking should be moved to \c serialCommunication
		 */
		class imuProcessor : public processor {
			
		public:
			/** \struct PTAMPacket_t
			 *  \brief Struct representing the incoming data from PTAM
			 */
			struct IMUPacket_t {
				uint8_t Header;
				float roll;
				float pitch;
				uint16_t Checksum;
			} __attribute__((packed));
			
			
		public:
			/** \brief Constructor
			 *
			 *  \details Takes the vehicle name as a parameter which could be used
			 *  for identification if several sensors are using the same \c listener port
			 *  \param _vehicleName The name of the vehicle
			 */
			imuProcessor(std::string _vehicleName) : processor(_vehicleName) {
				
			}
			
			
			/** \brief Virtual destructor */
			virtual ~imuProcessor() {}
			
			
			/** \brief Parses an incoming packet
			 *
			 *  \details Takes a char buffer and the size of the buffer and parses
			 *  the packet if one full buffer has arrived.
			 *  \param _message The incoming packet
			 *  \param _nbytes The number of incoming bytes
			 *  \return True if one message was successfully parse. False otherwise
			 *
			 *  \todo Move assumption of serial communication to the \c serialCommunication class
			 */
			virtual bool parse( const char* _message, size_t _nbytes ) {
				
			  float* data;

			  data = (float*)_message;

				// TODO - convert to quaternion
				mRawSensorData.orientation[0] = data[0];
				mRawSensorData.orientation[1] = data[1];
				
				mNew = true;
				
				return true;
			}
			
			
			/** \brief Checks for tracking
			 *
			 *  \return True if PTAM has reported good tracking
			 */
			virtual bool hasTracking() {
				return true;
			}
			
			
			/** \brief Checks for scale
			 *
			 *  \return True if PTAM has reported it has scale
			 */
			virtual bool hasScale() {
				return false;
			}
			
			
		private:
			IMUPacket_t mPacket;
			
		};
		
	} // namespace pars
} // namespace sefu

#endif // _IMUPROCESSOR_H_
