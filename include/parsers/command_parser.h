/** \file commander.h
 *  \brief Files containing the base class for commanders
 *  \author Niklas Bergström
 */
#ifndef _COMMANDER_H_
#define _COMMANDER_H_

#include "../sensor.h"

#include "../parsers.h"

namespace sefu {

	class sensorFusion;
	
    /** \class commander commander.h "commander.h"
     *  \brief Base class for commanders
     *  \author Niklas Bergström
     *  \version 0.1
     *  \ingroup Commanders
     *
     *  \details The commander class and its subclasses
     *  are intended for communication back to a physical
     *  sensor. An instance of the class should be provided,
     *  if required, at the creation of a listener
     */
	class command_parser : public parser {
		
	public:
		virtual bool parse( const char* _message, size_t _nbytes ) ;
		
		
		void registerSensors(std::vector<boost::shared_ptr<sensor> >& _sensors,
							 sensorFusion* _sf) {
			mSensors = _sensors;
			mSF = _sf;
		}
		
		virtual ~command_parser() {}
		
	private:
		char mCommand;
		
		std::vector<boost::shared_ptr<sensor> > mSensors;
		sensorFusion* mSF;
    };
	
} // namespace sefu

#endif // _COMMANDER_H_