/** \file filters.h
 *  \brief Convenience header for filter related files
 *  \author Niklas Bergström
 *
 *  \details
 */
#ifndef _FILTERS_H_
#define _FILTERS_H_

#include "filters/null_filter.h"
#include "filters/ptam_filter.h"
#include "filters/vicon_filter.h"

#include "sf_exception.h"

/** \defgroup Filters Sensor Fusion Filters
 *  This group contains classes subclassing from filter
 */

/** namespace filter */
namespace sefu {
    /** namespace sefu::filt */
    namespace filt {
        
        /** \fn inline filter* getFilter( const std::string& _id )
         *  \brief Returns a filter
         *  \ingroup Filters
         *
         *  \details Given the type of the filter id, the corresponding filter
         *  will be initialized and returned
         *
         *  \param _id String holding the type of the filter
         *  \return An initialized filter
         *  \throw An sfException if no filter with the provided id was found
         */
        inline filter* getFilter( const std::string& _id ) {
            filter* f = NULL;
            
            if( !_id.compare("vicon") ) {
                f = new viconFilter();
            } else if (!_id.compare("null")) {
                f = new nullFilter();
            } else if (!_id.compare("ptam")) {
                f = new ptamFilter();
            }
            
            if (!f) {
                throw sfException("Failed getting filter with id",_id);
            }
            
            return f;
        }
        
    } // namespace filt
} // namespace sefu

#endif // _FILTERS_H_
