#ifndef _SFEXCEPTION_H_
#define _SFEXCEPTION_H_

#include <exception>
#include <iostream>
#include <sstream>
#include <string>

#ifdef __unix
#define _NOEXCEPT _GLIBCXX_USE_NOEXCEPT
#endif

namespace sefu {
    
    class sfException : public std::exception {
    public:
        sfException( std::string _message, std::string _additional = "" ) {
            std::stringstream ss;
            ss << _message;
            if( _additional.length() > 0 ) {
                ss << " ";
                ss << _additional;
            }
            mMessage = ss.str();
        }
        
        virtual const char* what() const _NOEXCEPT {
	    return mMessage.c_str();
	}
    
private:
    std::string mMessage;
};

} // namespace sefu

#endif // _SFEXCEPTION_H_
