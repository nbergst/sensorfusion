/** \file sensor.h
 *  \brief Files containing the sensor class
 *  \author Niklas Bergström
 */

#ifndef _SENSOR_H_
#define _SENSOR_H_

#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include <iterator>
#include <queue>

#include "communication/communication.h"
#include "filters/filter.h"
#include "parsers/command_parser.h"

#include "command_receivers.h"

#include "coordinate_transform.h"

namespace sefu {
	
	enum eSensorRole {
		SENSOR = 0,
		REFERENCE
	};
	
	enum eCoordinateFrame {
		GLOBAL = 0,
		LOCAL
	};
	
	/** \class sensor sensor.h "sensor.h"
	 *  \brief Class representing a hardware sensor
	 *  \author Niklas Bergström
	 *  \version 0.1
	 *
	 *  \details The sensor class represents a hardware sensor, such
	 *  as PTAM, Vicon or Odometry. An instance of a sensor needs
	 *  a listener
	 */
	class sensor : public pars::processorCallback {
		
	public:
		/** \brief Initializes non-valid sensor */
		sensor();
		
		/** \brief Initializes sensor
		 *
		 *  Default constructor for the sensor class. Takes
		 *  a pointer to an initialized litener
		 *  and the type of the sensor ("vicon", "ptam" etc.)
		 *
		 *  \param _l Listener (cannot be NULL)
		 *	\param _f Filter (cannot be NULL)
		 *  \param _type Sensor type
		 */
		sensor(comm::listener<pars::processor>* _l, filt::filter* _f, cmrc::commandReceiver* _cr, const std::string& _type);
		
		
		/** \brief Initializes sensor
		 *
		 *  Default constructor for the sensor class. Takes
		 *  a pointer to an initialized litener and a transmitter
		 *  and the type of the sensor ("vicon", "ptam" etc.)
		 *
		 *  \param _l Listener (cannot be NULL)
		 *  \param _t Transmitter (cannot be NULL)
		 *	\param _f Filter (cannot be NULL)
		 *  \param _type Sensor type
		 */
		sensor(comm::listener<pars::processor>* _l, comm::transmitter* _t, filt::filter* _f, cmrc::commandReceiver* _cr, const std::string& _type);
		
		
		/** \brief Destructor */
		virtual ~sensor() { stop(); }
		
		
		/** \brief Method for checking if sensor is valid
		 *
		 *  \return true if sensor is valid
		 */
		bool valid() const;
		
		
		/** \brief Starts listening to sensor */
		void start();
		
		
		/** \brief Stops listening to sensor */
		void stop();
		
		
		/** \brief Gets the type of the sensor
		 *
		 *  \return Sensor type
		 */
		const std::string& type() const;
		
		/** \brief Gets the id of the sensor
		 *
		 *  \return id of the sensor
		 */
		int getId() const;
		
		
		/** \brief Gets raw sensor data
		 *
		 *  \return Raw sensor data
		 */
		const SensorStruct& getRawData();
		
		
		/** \brief Gets filtered sensor data
		 *
		 *  \return Filtered sensor data
		 */
		const SensorStruct& getFilteredData();
		
		
		/*		void setNeedsInit() {
			resetTransform();
			mTransform.setNeedsInit();
			}*/


		// Will keep?
		bool hasTracking();
		bool hasScale();
		
		// Needed?
		void setPosition( float x, float y, float z ) {
			mRawSensorData.position[0] = x;
			mRawSensorData.position[1] = y;
			mRawSensorData.position[2] = z;
		}
		void setOrientation( float q0, float q1, float q2, float q3 ) {
			mRawSensorData.orientation[0] = q0;
			mRawSensorData.orientation[1] = q1;
			mRawSensorData.orientation[2] = q2;
			mRawSensorData.orientation[3] = q3;
		}
		
		
		// Do quaternion conversion
		void setOrientation( float roll, float pitch, float yaw) {
		}
		
		
		float x() const { return mRawSensorData.position[0]; }
		float y() const { return mRawSensorData.position[1]; }
		float z() const { return mRawSensorData.position[2]; }
		
		
		// Do quaternion conversion
		float heading() { return 0.0f; }
		
		
		bool hasNew() { 
		  bool b = mHasNew;
		  //		  mHasNew = false;
		  return b;
		}
		
		void setHasNotNew() {
		  mHasNew = false;
		}
		
		bool operator==(const sensor& _rhs ) const {
			return !(this->mType.compare(_rhs.mType));
		}

		bool operator!=(const sensor& _rhs ) const {
			return !operator==(_rhs);
		}
		
		bool canUse(const boost::shared_ptr<sensor>& _ref) ;

		eSensorRole role() const { return mRole; }
		
		eCoordinateFrame frame() const { return mCoordinateFrame; }
		
		size_t timestamp() const;

		bool compareRollAndPitch(const boost::shared_ptr<sensor> _reference) const;
		
		double confidence() const;
		
		short quality(const boost::shared_ptr<sensor> _reference = boost::shared_ptr<sensor>() ) const;
		
		void toggleQualityOverride();

		void processCommand(char _c);
		
		void fillPoseUntransformed(float position[3], float orientation[4]);
		void fillPose(float position[3], float orientation[4]) ;
		
		void sendMessage(char _message);

		void configure(const boost::property_tree::ptree& _pt,
					   const std::string& _thisSensor);

		void setupTransform(const boost::shared_ptr<sensor>& _ref);
		void initTransform(const boost::shared_ptr<sensor>& _ref);
		void updateTransform(const boost::shared_ptr<sensor>& _ref);
		void resetTransform();
		Vector3 position() const;
		Quaternion orientation() const;

		void printTransform() const;

		Vector3 transformedPosition(const Quaternion& _refq) const;
		Quaternion transformedOrientation() const;

		Vector3 sigmaP() const { return mSigmaP; }
		Vector3 sigmaR() const { return mSigmaR; }
		
		/** \brief Message for the logger */
		std::string logMessage(bool b=false);
		std::string logMessageRaw(bool b);
		
	private:
		/** \brief Callback for listener
		 *
		 *  This method is called each time new data is received by the listener
		 */
		void processNewData(pars::processor* _processor);
		
	private:
		// The sensor's type
		std::string mType;
		// Handles incoming communication and parsing
		boost::shared_ptr<comm::listener<pars::processor> > mListener;
		// Handles outgoing communication
		boost::shared_ptr<comm::transmitter> mTransmitter;
		// Filter for the incoming data
		boost::shared_ptr<filt::filter> mFilter;
		
		boost::shared_ptr<cmrc::commandReceiver> mCommandReceiver;
		
		// Name identifying the vehicle attached to the sensor
		std::string mName;
		
		// Beliefs of the sensor
		SensorStruct mRawSensorData;
		
		// Indicates if we have new data
		volatile bool mHasNew;
		
		// For locking parsing and retrieval of data
		boost::mutex mLock;
		
		// Indicates whether the sensor is inited
		bool mInited;
		
		/** Inticates the role of the sensor */
		eSensorRole mRole;
		
		eCoordinateFrame mCoordinateFrame;
		
		/** Inticates whether the sensor has an absolute scale */
		bool mFixedScale;

		// Measurement noise parameters
		Vector3 mSigmaP;
		Vector3 mSigmaR;
		
		// Process noise parameters
		Vector3 mProcessP;
		
		// Transform to reference frame
		Vector3 mPositionOffset;
		Quaternion mRotationOffset;

	    Quaternion mRefQuaternion;
	    
		int mReinitCounter;

	public:
		// Transform to specific frame
		coordinateTransform mTransform;

		// Manually override quality measure
		bool mQuality;
		
	};
	
	typedef boost::shared_ptr<sensor> sensor_t;
	
	
	class sensorIterator : public std::iterator<std::input_iterator_tag, std::vector<sensor_t > > {
		
		std::vector<sensor_t>& p;
		int idx;
		bool first;
		
	public:
		sensorIterator(std::vector<sensor_t>& x) : p(x), idx(0) {
			while( idx < p.size() && p[idx]->role() != eSensorRole::SENSOR	) idx++;
			first = idx < p.size();
		}
		
		sensorIterator(const sensorIterator& mit) : p(mit.p) {}
		
		sensorIterator& operator++() {
			idx++;
			
			while( idx < p.size() && p[idx]->role() != eSensorRole::SENSOR	) idx++;

			return *this;
		}
		
		bool ended() {
			if( first ) {
				first = false;
				return false;
			}
			int tidx = idx;
			while( tidx < p.size() && p[tidx]->role() != eSensorRole::SENSOR) {
				tidx++;
			}
			return tidx >= p.size();
		}
		
		sensorIterator operator++(int) {
			sensorIterator tmp(*this);
			operator++();
			return tmp;
		}
		
		bool operator==(const sensorIterator& rhs) {return &p == &(rhs.p);}
		
		bool operator!=(const sensorIterator& rhs) {return &p != &(rhs.p);}
		
		sensor_t operator*() {
			sensor_t s = p[idx];
			return s;
		}
	};
	
} // namespace sefu

#endif // _SENSOR_H_
