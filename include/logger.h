#ifndef _LOGGER_H_
#define _LOGGER_H_

#include <string>
#include <boost/function.hpp>

namespace sefu {
    
    class logger {
    public:
		logger( boost::function<bool ()> f, int _freq ) :
		mLog(f), mFrequency(_freq) {
		}
        void operator()() {
            long sleepTime = 1000000/mFrequency;
                
            while (true) {

		
                // Record time before sending
                boost::posix_time::ptime t1 = boost::posix_time::microsec_clock::local_time();
                
                // Send
				if(!mLog()) {
					break;
				}
				
                // Record time after sending
                boost::posix_time::ptime t2 = boost::posix_time::microsec_clock::local_time();
                boost::posix_time::time_duration td = t2-t1;
                
                boost::chrono::microseconds sleep_duration{sleepTime-td.total_microseconds()};
                
                // Sleep
                boost::this_thread::sleep_for(sleep_duration);
            }
            
        }
        
        
    private:
		boost::function<bool ()> mLog;
        int mFrequency;
    };
    
} // namespace sefu
    
#endif // _LOGGER_H_
