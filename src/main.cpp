#include "sensor_fusion.h"

#include <boost/thread.hpp>

#include <boost/interprocess/shared_memory_object.hpp>

using namespace sefu;

//class InputHandler : public boost::enable_shared_from_this<InputHandler> {
//	
//public:
//	typedef boost::shared_ptr<InputHandler> InputHandler_t;
//	
//	static void create( boost::asio::io_service& _service, sensorFusion& _sf ) {
//		InputHandler_t input( new InputHandler( _service, _sf ) );
//		input->read();
//	}
//	
//private:
//	explicit InputHandler( boost::asio::io_service& _service, sensorFusion& _sf ) :
//	mInput( _service ), mSF(_sf) {
//		mInput.assign(STDIN_FILENO);
//	}
//	
//	void read() {
//		std::cout << "> " << std::flush;
//		boost::asio::async_read(mInput,
//								boost::asio::buffer( &mCommand, sizeof(mCommand) ),
//								boost::bind( &InputHandler::read_handler,
//											shared_from_this(),
//											boost::asio::placeholders::error,
//											boost::asio::placeholders::bytes_transferred));
//	}
//	
//	
//	void read_handler( const boost::system::error_code& _error,
//					  const size_t _nbytes ) {
//		if( _error ) {
//			std::cerr << "Read error: " << boost::system::system_error(_error).what() << std::endl;
//			return;
//		}
//		switch( mCommand ) {
//			case 'e':
//				mSF.toggleLocalEcho();
//				std::cout << "Toggling local echo\n";
//				break;
//			case 'q':
//				mSF.stop();
//				return;
//				
//			case '0':
//			case '1':
//			case '2':
//			case '3':
//			case '4':
//			case '5':
//			case '6':
//				mSF.toggleQuality(mCommand);
//				break;
//				
//			case 'v':
//				mSF.setViconMode();
//				break;
//			case 'p':
//				mSF.setPTAMMode();
//				break;
//				
//			case 'r':
//				mSF.resetPTAM();
//				break;
//				
//			case 'f':
//				mSF.setFullFusion();
//				break;
//				
//			case 'z':
//		  //mSF.resetTransformations();
//		  break;
//				
//			case '\n':
//				break;
//				
//			default:
//				std::cout << "Unknown command " << mCommand << " (" << (int)mCommand << ")\n";
//				break;
//		}
//		
//		this->read();
//	}
//	
//private:
//	boost::asio::posix::stream_descriptor mInput;
//	char mCommand;
//	sensorFusion& mSF;
//};


int main( int _argc, char** _argv ) {

	// Initialize sensor fusion with config file
	sensorFusion sf = _argc > 1 ? sensorFusion(_argv[1]) : sensorFusion("config.ini");
	
	// Create input handler
	//boost::asio::io_service io;
	//InputHandler::create(io,sf);
	
	// Run sensor fusion
	boost::thread fusionThread(boost::ref(sf));
	
	// Run input handler
	//io.run();
	
	// Wait for thread to stop
	fusionThread.join();
	std::cout << "Exiting...\n";
	
	//io.stop();
	
	return 0;
}



