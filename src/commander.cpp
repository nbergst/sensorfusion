//
//  commander.cpp
//  sensor_fusion
//
//  Created by Niklas Bergström on 2014-09-11.
//
//

#include "parsers/command_parser.h"

#include "sensor_fusion.h"

namespace sefu {
	
	bool command_parser::parse( const char* _message, size_t _nbytes ) {
		mCommand = *_message;
		
		std::cout << "received: " << mCommand << std::endl;
		
		for( int i=0; i<mSensors.size(); ++i ) {
			mSensors[i]->processCommand(mCommand);
		}
		
		mSF->processCommand(mCommand);
		
		return true;
	}
	
}
