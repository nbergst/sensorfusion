#include "sensor_fusion.h"

#include <boost/date_time/date_clock_device.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread.hpp>

#include "parsers.h"
#include "filters.h"
#include "communications.h"
#include "command_receivers.h"

#include "logger.h"

char help_text[] = "Available commands: \n" \
"e: Toggles local echo of logging messages\n" \
"f: Full sensor fusion\n" \
"h: Prints this help text\n" \
"p: Sets PTAM priority mode\n" \
"q: Quit\n" \
"v: Sets Vicon mode\n";


namespace sefu {
	
	using namespace boost::interprocess;
	using namespace boost::gregorian;
	using namespace boost::posix_time;
	
	// sefu::comm
	using namespace comm;
	// sefu::proc
	using namespace pars;
	// sefu::filt
	using namespace filt;
	// sefu::cmrc
	using namespace cmrc;
	
	
	// Construct and initialize fusion
	sensorFusion::sensorFusion(const std::string& _configFilez)  {
	  
	  FILE* f = fopen((const char*)_configFilez.c_str(),"r");
	  
	  std::string _configFile = _configFilez;
	  if( !f ) {
		_configFile = "/home/swarm/config.ini";
	  } else {
		fclose(f);
	  }
	 
	  std::cout << "Using config file: " << _configFile << std::endl;
		
		// Open config file and build property tree
		boost::property_tree::ptree pt;
		try {
			boost::property_tree::ini_parser::read_ini(_configFile, pt);
		} catch (boost::property_tree::ptree_error e) {
		  std::cerr << "Couldn't find " << _configFile << "\nExiting...\n" << e.what() << std::endl;			try {
				boost::property_tree::ini_parser::read_ini( "../" + _configFile, pt);
			} catch (boost::property_tree::ptree_error e) {
			  std::cerr << "Couldn't find " << _configFile << "\nExiting...\n" << e.what() << std::endl;
			  exit(-1);
			}
		}
		
		// Init some variables
		mLocalEcho = false;
		mDisplayTransformed = true;
		mForcePTAM = false;
		mForceOdo = false;
		
		// Parse property tree and initialize variables
		try {
			// Vehicle name
			mName = pt.get<std::string>("vehicle.name");
			
			// Set default sensor fusion mode
			mMode = (eFusionMode)pt.get<int>("fusion.mode");
			
			// Setup sensors
			int nSensors = pt.get<int>("sensors.number");
			mNSensors = 0;
			// ======================= Set up each sensor
			for( int i=0; i<nSensors; ++i ) {
				// ======================= Get info from config file
				// Enumerate sensors
				std::stringstream ss;
				ss << "sensor" << i;
				// Get connection type and create sensor accordingly
				std::string communication = pt.get<std::string>(ss.str() + ".communication");
				std::string sensorType = pt.get<std::string>(ss.str() + ".type");
				
				communicator* l = NULL;
				
				// ======================= Set up processor
				processor* p = getProcessor(sensorType, mName);
				if( !p ) {
					std::cerr << "Found no processor for " << sensorType << " sensor." << std::endl;
					exit(-1);
				}
				
				// ======================= Set up filter
				filt::filter* f = getFilter(pt.get<std::string>(ss.str() + ".filter"));
				if( !p ) {
					std::cerr << "Found no filter for " << sensorType << " sensor." << std::endl;
					exit(-1);
				}
				
				// ======================= Set up listener/duplex
				if( !pt.get<std::string>(ss.str() + ".direction").compare("simplex") ) {
					l = getListener(communication,
									pt.get<std::string>(ss.str() + ".address"),
									pt.get<int>(ss.str() + ".port"),
									sensorType,
									p);
				} else {
				  std::cout << "getting duplex for: " << sensorType << std::endl;
					l = getDuplex(communication,
								  pt.get<std::string>(ss.str() + ".address"),
								  pt.get<int>(ss.str() + ".port"),
								  sensorType,
								  p);
				}
				
				// ======================= Set up command receivers
				commandReceiver* cr = getCommandReceiver(sensorType);
				
				
				// ======================= Initialize sensor
				// If we could initialize a sensor, add it to the sensor vector
				if( l ) {
					// Add sensor to vector
					if( !pt.get<std::string>(ss.str() + ".direction").compare("simplex") ) {
						mSensors.push_back(boost::shared_ptr<sensor>(new sensor(dynamic_cast<listener<pars::processor>*>(l),f,cr,sensorType)));
					} else {
					  std::cout << "constructing with transmitter for " << sensorType << std::endl;
						mSensors.push_back(boost::shared_ptr<sensor>(new sensor(dynamic_cast<listener<pars::processor>*>(l),dynamic_cast<transmitter*>(l),f,cr,sensorType)));
					}
					mSensors.back()->configure(pt,ss.str());
					mSensorIDs.push_back(sensorType);
					mNSensors++;
					std::cout << "Adding sensor: " << sensorType << std::endl;
				} else {
					std::cerr << "Could not initialize " << sensorType << " sensor." << std::endl;
//					exit(-1);
				}
			}
			
			
			std::cout << "Could connect to " << mNSensors << " sensor" << (mNSensors == 1 ? "." : "s.") << std::endl;
			
			
			// ======================= Initialize transformations betseen sensors
			mActiveSensor = mSensors[0];
			for( auto sensor : mSensors ) {
				sensor->setupTransform(mActiveSensor);
			}
			
			
			// ======================= Set up transmission to vehicle
			std::string address = pt.get<std::string>("vehicle.address");
			int port = pt.get<int>("vehicle.port");
			switch (pt.get<int>("vehicle.type")) {
				case 0: // Multicast
					mVehicleTransmitter =  newTransmitter<udpTransmitter/*<VehiclePacket_t>*/ >(address,port);
					break;
					
				case 1: // Shared memory
					mVehicleTransmitter =  newTransmitter<shmTransmitter<VehiclePacket_t> >(address,port);
					break;
					
				case 2: // Serial
					mVehicleTransmitter =  newTransmitter<serialTransmitter/*<VehiclePacket_t>*/ >(address,port);
					break;
					
				default:
					break;
			}
			
			
			// ======================= Set up transmission for logging to UI
			address = pt.get<std::string>("logger.address");
			port = pt.get<int>("logger.port");
			switch (pt.get<int>("logger.type")) {
				case 0: // Multicast
					mGUILogger =  newTransmitter< udpTransmitter/*<LoggingPacket_t>*/ >(address,port);
					break;
					
				case 1: // Shared memory
					// No logging yet with shared memory
					//mGUILogger = newTransmitter<shmTransmitter<LoggingPacket_t> >(address,port);
					std::cerr << "Cannot use interprocess communication for transmitter\n";
					exit(-1);
					break;
					
				case 2: // Serial
					mGUILogger =  newTransmitter< serialTransmitter/*<LoggingPacket_t>*/ >(address,port);
					break;
					
				default:
					break;
			}
			
			
			// ======================= Set up file for logging
			std::stringstream datestream;
			date current_date( boost::date_time::day_clock<date>::local_day() );
			std::string datestring = to_iso_string(current_date);
			
			time_facet* facet(new time_facet("%Y%m%d_%H%M%S"));
			datestream.imbue(std::locale(datestream.getloc(), facet));
			datestream << second_clock::local_time();
			std::string logfileName = pt.get<std::string>("logfile.name") + "__" + pt.get<std::string>("vehicle.name") + "_" + datestream.str() + ".csv";
			
			std::cout << "Logging to: " << logfileName << std::endl;
			mFileLogger = newTransmitter<fileTransmitter>(logfileName,0);
			
			// ======================= Set up command receiver
			command_parser* c = new command_parser();
			
			c->registerSensors(mSensors,this);
			
			listener<sefu::command_parser>* cl = getCommanderListener( pt.get<std::string>("commander.type"), pt.get<std::string>("commander.address"), pt.get<int>("commander.port"), c);
			
			mCommandReceiver = boost::shared_ptr<comm::listener<sefu::command_parser> >(cl);
			
			mCommandReceiver->start();
			
			
		} catch (boost::property_tree::ptree_error e) {
			std::cerr << "There was an error parsing " << _configFile << ". (Error: " << e.what() << ") Exiting.\n";
			exit(-1);
		} catch (std::exception& e) {
			std::cerr << "Exception: " << e.what() << ". Exiting\n";
			exit(-1);
		}
	}
	
	
	void sensorFusion::processCommand(char _message) {
		
		switch(_message) {
			case 'p':
				setPTAMMode();
				break;
			case 'v':
				setViconMode();
				break;
			case 'o':
				setOdometryMode();
				break;
				

			case 'q':
				stop();
				break;
				
			case 'e':
				toggleLocalEcho();
				break;
			case 'h':
				std::cout << help_text << std::endl;
				break;
				
			case 'w':
				toggleUIOutput();
				break;
				
			case '0':
			  mForcePTAM = false;
			  mForceOdo = false;
			  break;
			case '1':	
			  mForcePTAM = true;
			  mForceOdo = false;
			  break;
		case '2':
			  mForcePTAM = false;
			  mForceOdo = true;
			  break;
		case '3':
			case '4':
			case '5':
			case '6':
				toggleQuality(_message);
				break;
				
			default:
				
				break;
		}
	}
	
	
	// Transmit to vehicle
	void sensorFusion::transmitToVehicle() const {
		mVehicleTransmitter->sendRaw(mToSend);
	}
	
	
	
	// Transmit to GUI
	bool sensorFusion::logToGUI() const {
		int active = 0;
		if( sensorWithID("vicon") == mActiveSensor ) {
			active = 0;
		} else if( sensorWithID("ptam") == mActiveSensor ) {
			active = 1;
		} else {
			active = 2;
		}
		
		
		std::stringstream ss;
		ss << mName << " " << "0.0," << active << ";";
		
		for (int i=0; i<mSensors.size(); ++i) {
			if( mDisplayTransformed ) {
			  ss << mSensors[i]->logMessage(false) << ";";
			} else {
			  ss << mSensors[i]->logMessageRaw(false) << ";";
			}
		}
		
		mGUILogger->sendString(ss.str());
		
		if( mLocalEcho ) {
			std::cout << std::fixed << std::setprecision(3);
			for (int i=0; i<mSensors.size(); ++i) {
				std::cout << mSensors[i]->type() << " pos.: " << mSensors[i]->transformedPosition(mActiveSensor->transformedOrientation()) << std::endl;
				std::cout << mSensors[i]->type() << " ori.: " << mSensors[i]->transformedOrientation() << std::endl;
			}
		}
		
		//std::cout << ss.str() << std::endl;
		
		return mRunning;
	}
	
	int activeID(const std::string& _s) {
		
		if( !_s.compare("vicon") ) return 0;
		if( !_s.compare("ptam") ) return 1;
		if( !_s.compare("odo") ) return 2;
		
		return -1;
	}
	// Save to log-file
	bool sensorFusion::logToFile() const {
		
		std::stringstream ss;
		//		ss << mName << ";";
		
		ss << activeID(mActiveSensor->type()) << ",";
		
		Quaternion qref = mActiveSensor->transformedOrientation();

		for( int i=0; i<mSensors.size(); ++i ) {
			ss << mSensors[i]->position()[0] << ",";
			ss << mSensors[i]->position()[1] << ",";
			ss << mSensors[i]->position()[2] << ",";
			ss << mSensors[i]->orientation()[0] << ",";
			ss << mSensors[i]->orientation()[1] << ",";
			ss << mSensors[i]->orientation()[2] << ",";
			ss << mSensors[i]->orientation()[3] << ",";
			ss << mSensors[i]->transformedPosition(qref)[0] << ",";
			ss << mSensors[i]->transformedPosition(qref)[1] << ",";
			ss << mSensors[i]->transformedPosition(qref)[2] << ",";
			ss << mSensors[i]->transformedOrientation()[0] << ",";
			ss << mSensors[i]->transformedOrientation()[1] << ",";
			ss << mSensors[i]->transformedOrientation()[2] << ",";
			ss << mSensors[i]->transformedOrientation()[3] << ",";
			ss << mSensors[i]->mTransform.s_hat << ",";
			ss << mSensors[i]->mTransform.r_hat[0] << ",";
			ss << mSensors[i]->mTransform.r_hat[1] << ",";
			ss << mSensors[i]->mTransform.r_hat[2]  << ",";
			ss << mSensors[i]->mTransform.t_hat[0]  << ",";
			ss << mSensors[i]->mTransform.t_hat[1]  << ",";
			ss << mSensors[i]->mTransform.t_hat[2] << ",";
		}
		
		ss << "\n";
		
		mFileLogger->sendString(ss.str());
		
		return mRunning;
	}
	
	
	void sensorFusion::viconMode() {
	  mActiveSensor = sensorWithID("vicon");
		singleSensorMode("vicon");
	}
	
	
	void sensorFusion::ptamMode() {
		mActiveSensor = sensorWithID("ptam");
		singleSensorMode("ptam");
	}
	
	
	void sensorFusion::odoMode() {
	  mActiveSensor = sensorWithID("odo");
		singleSensorMode("odo");
	}
	
	
	void sensorFusion::singleSensorMode(const std::string& _s) {
		sensor_t s = sensorWithID(_s);
		
		if( s->hasNew() ) {
			mToSend.quality = s->quality();
			s->fillPoseUntransformed(mToSend.position, mToSend.orientation);
		}
	}
	
	
	
	sensor_t sensorFusion::full(sensor_t _current) {

		sensor_t nextSensor;
		double confidence = 0.0;
		// Should be generic
		for( auto it = sensorIterator(mSensors); !it.ended(); ++it ) {
		  //
			if( (*it)->quality() && (*it)->confidence() > confidence && (*it)->canUse(_current) ) {
				confidence = (*it)->confidence();
				nextSensor = *it;
			} else {
			  //std::cout << (*it)->quality() << " " <<  (*it)->confidence() << " " << (*it)->canUse(_current) << std::endl;
			}
		}
		
		
		return nextSensor;
	}
	
	
	// FULL SENSOR FUSION
	int sensorFusion::full(int state) {
		
	  //std::cout << "checking for new sensor. currently in state " << state << std::endl;
		if( sensorWithID("vicon")->quality() && !mForcePTAM && !mForceOdo) {
			if( state == 0 || sensorWithID("vicon")->canUse(mActiveSensor) ) {
				mActiveSensor = sensorWithID("vicon");
				if( sensorWithID("ptam")->quality() ) {
				  sensorWithID("ptam")->canUse(mActiveSensor);
		     	}
				return 0;
			} else if( sensorWithID("ptam")->quality() && sensorWithID("ptam")->canUse(mActiveSensor) ) {
			  mActiveSensor = sensorWithID("ptam");
			  return 1;
			} else {
				mActiveSensor = sensorWithID("odo");
				return 2;
			}
		} else if( state != 3 && sensorWithID("ptam") && sensorWithID("ptam")->quality() && !mForceOdo) {
		  if( sensorWithID("ptam")->canUse(mActiveSensor) ) {
			mActiveSensor = sensorWithID("ptam");
			return 1;
		  } else {
			mActiveSensor = sensorWithID("odo");
			return 2;
		  }
		} else {
			mActiveSensor = sensorWithID("odo");
			return 2;
		}
		return -1;
	}
	
	
	void sensorFusion::operator()() {
		mRunning = true;
		
		// Setup logging to gui
		//		boost::thread guilog_thread(logger(boost::bind(&sensorFusion::logToGUI,this),10));
		
		// Setup logging to file
		//boost::thread filelog_thread(logger(boost::bind(&sensorFusion::logToFile,this),10));
		
		for( int i=0; i<mSensors.size(); ++i ) {
			mSensors[i]->start();
			std::cout << mSensors[i]->type() << " started\n";
		}
		
		// Make sure we have Vicon data
		// while( !mSensors[0]->hasNew() ) { usleep(1000); } --- This shouldn't be needed
		
		
		// Get name and id of vehicle to send to vehicle wrapper
		memcpy(mToSend.name,mName.c_str(),mName.length());
		mToSend.name[mName.length()] = '\0';
		
		// Sleeptime in usec. Set to 100 Hz
		long sleepTime = 1000000/100;
		
		// Cannot rely on the first sensor being alive
		/*for( int i=1; i<mSensors.size(); ++i ) {
			for( int j=0; j<40; ++j ) {
				while( !mSensors[i]->hasNew() ) { usleep(1000); }
				mSensors[i]->setHasNotNew();
			}
			
			std::cout << "Initing transform for " << mSensors[i]->type() << "\n";
			mSensors[i]->initTransform(mSensors[0]);
		}*/
		
		// Timestamp to send to vehicle wrapper
		size_t timestamp = 100000;
		
		// Initially we don't have an active sensor
		int activeID = -1;
		mActiveSensor = sensor_t();

		std::cout << "Setup complete. Starting loop\n";
		
		while( mRunning ) {
			
			// Record time before sending
			boost::posix_time::ptime t1 = boost::posix_time::microsec_clock::local_time();
			
			// Check if mode has changed
			int tID = 0;
			sensor_t tSensor;
			switch(mMode) {
				case viconOnlyMode:
					viconMode();
					break;
					
				case ptamOnlyMode:
					ptamMode();
					break;
					
				case odoOnlyMode:
					odoMode();
					break;
			  
				case fullFusionMode:
				  //					tID = full(activeID);
					tSensor = full(mActiveSensor);
					break;
					
				default:
					break;
			}
			
			
			if(!tSensor) {
				std::cerr << "No sensor available\n";
				sleep(1);
				continue;
			}
			
			//			std::cout << "active id: " << activeID << " new id: " << tID << std::endl;

			// ================== Update all transformations
			// update transform
			/*			if( activeID != tID ) {
			  std::cout << "switched from " << (mSensors[activeID]->type() << " to " << mSensors[tID]->type() << "\n";
			  }*/
			if( mActiveSensor != tSensor ) {
			  std::cout << "Switched from " << (!mActiveSensor ? "no sensor" : mActiveSensor->type()) << " to " << tSensor->type() << "\n";
			  mToSend.id = 100;//mSensors[0]->getId();
			}
			
			// Update sensors
			auto it = sensorIterator(mSensors);
			for( ; !it.ended(); ++it ) {
				if( tSensor->hasNew() && (*it)->hasNew() && (*it)->quality() ) {
					// Do not update if the sensor is active or has a global reference frame
					if( *tSensor == *(*it) || (*it)->frame() == eCoordinateFrame::GLOBAL ) {
						continue;
					}
					(*it)->updateTransform(tSensor);
				}
			}
			
			/*
			switch (tID) {
				case 0:
				  if( activeID != 0 )
					std::cout << "switched from " << activeID << " to vicon\n";
				  // TODO Need to re-initialize transform if switching back from odometry?
				  if( mActiveSensor->hasNew() && sensorWithID("ptam") && sensorWithID("ptam")->hasNew() && sensorWithID("ptam")->quality() ) {
					sensorWithID("ptam")->updateTransform(mActiveSensor);
				  }
				  if( mActiveSensor->hasNew() && sensorWithID("odo") && sensorWithID("odo")->hasNew() && sensorWithID("odo")->quality() ) {
					sensorWithID("odo")->updateTransform(mActiveSensor);
				  }
				  state = 0;
				  break;
				  
			case 1:
			  if( activeID != 1 )
				std::cout << "switched from " << activeID << " to ptam\n";
			  if( mActiveSensor->hasNew() && sensorWithID("odo") && sensorWithID("odo")->hasNew() && sensorWithID("odo")->quality() ) {
				sensorWithID("odo")->updateTransform(mActiveSensor);
				
				
			  }
			  state = 1;
			  break;
			  
			case 2:
			  if( activeID != 2 )
				std::cout << "switched from " << activeID << " to odo\n";
			  // If we went from ptam to odo, it means that ptam was lost. ptam is trying to re-initialize, and when it does we need to start transformation with odo
			  if( mActiveSensor->hasNew() && sensorWithID("ptam") && sensorWithID("ptam")->hasNew() && sensorWithID("ptam")->quality() ) {
				std::cout << "updating ptam with odometry\n";
				sensorWithID("ptam")->updateTransform(mActiveSensor);
			  }
			  state = 2;
			  break;
			  
			default:
			  break;
			}
			 */
			activeID = tID;
			mActiveSensor = tSensor;
			
			/*if( mActiveSensor->hasNew() && sensorWithID("vicon") && sensorWithID("vicon")->hasNew() && sensorWithID("vicon")->quality() ) {
			  sensorWithID("vicon")->updateTransform(mActiveSensor);
			  }
			 
			 if( tID != activeID ) {
			  std::cout << "switched\n";
			  activeID = full(tID);
			  }*/


			/*for( int i=mSensors.size()-1; i>=0; --i ) {
				// The active sensor should maintain its transform
				if( mActiveSensor.get() == mSensors[i].get() ) {
					//std::cout << "Not updating " << mActiveSensor->type() << std::endl;
					break;
				}
				// Only sensors currently having good data should get updated
				if( mActiveSensor->hasNew() ) {
					if( mSensors[i]->hasNew() )
						if( mSensors[i]->quality() ) {
							mSensors[i]->updateTransform(mActiveSensor);
						}
				}
				}*/
			
			// Use the position and orientation estimates from the active sensor
			if (mActiveSensor->hasNew()) {
				mToSend.quality = mActiveSensor->quality() >= 1;
				mActiveSensor->fillPose(mToSend.position, mToSend.orientation);
			} else {
			  //mToSend.quality = 0;
			}
			
			// Set time to fusion time
			mToSend.timestamp = timestamp;
			
			// Transmit to vehicle
			transmitToVehicle();
			
			// Log to file
			logToFile();
			
			// Record time after sending and sleep
			boost::posix_time::ptime t2 = boost::posix_time::microsec_clock::local_time();
			boost::posix_time::time_duration td = t2-t1;
			boost::chrono::microseconds sleep_duration{sleepTime-td.total_microseconds()};
			boost::this_thread::sleep_for(sleep_duration);
			
			timestamp += 1;
		}
		
		std::cout << "Stopping sensors...\n";
		for( int i=0; i<mSensors.size(); ++i ) {
			mSensors[i]->stop();
		}
		
		std::cout << "Stopping logging thread...\n";
		
		//guilog_thread.join();
		//filelog_thread.join();
		
		std::cout << "Exiting fusion...\n";
		
	}
	
	
	void sensorFusion::toggleQuality( char _c ) {
		
		int i = (int)_c - 48;
		mSensors[i]->toggleQualityOverride();
		
	}
	
	
	void sensorFusion::setViconMode()  {
		if( sensorWithID("vicon") ) {
			std::cout << "Using vicon only\n";
			mMode = viconOnlyMode;
			mActiveSensor = sensorWithID("vicon");
		} else {
			std::cout << "No Vicon sensor connected\n";
		}
	}
	
	void sensorFusion::setPTAMMode()  {
		if( sensorWithID("ptam") ) {
			std::cout << "Using ptam only\n";
			mMode = ptamOnlyMode;
			mActiveSensor = sensorWithID("ptam");
		} else {
			std::cout << "No PTAM sensor connected\n";
		}
	}
	
	
	void sensorFusion::setOdometryMode()  {
		if( sensorWithID("odo") ) {
			std::cout << "Using odometry only\n";
			mMode = odoOnlyMode;
			mActiveSensor = sensorWithID("odo");
		} else {
			std::cout << "No odometry sensor connected\n";
		}
	}
	
	
	void sensorFusion::setFullFusion() {
		std::cout << "Setting priority mode\n";
		mMode = fullFusionMode;
	}
	
	
	void sensorFusion::resetPTAM() {
		sensor_t ptam = sensorWithID("ptam");
		ptam->sendMessage('r');
	}
	
	
	void sensorFusion::resetTransformations() {
		
		for( int i=1; i<mSensors.size(); ++i ) {
			if( mSensors[0]->hasNew() && mSensors[i]->hasNew() ) {
				mSensors[i]->resetTransform();
			}
		}
		
	}
	
	
	
	boost::shared_ptr<sensor> sensorFusion::sensorWithID( const std::string& _type ) const {
		static boost::shared_ptr<sensor> s;
		for( unsigned int i=0; i<mSensors.size(); ++i ) {
			if( !mSensors[i]->type().compare(_type) ) {
				return mSensors[i];
			}
		}
		return s;
	}
	
	
	void sensorFusion::stop() {
		std::cout << "Stopping fusion thread\n";
		mRunning = false;
	}
	
} // namespace sefu
