#include "coordinate_transform.h"

#include <algorithm>
#include <iostream>
#include <numeric>

#include <cfloat>

namespace sefu {
	
	Vector3::Vector3() {
		v[0] = v[1] = v[2] = 0.0f;
	}
	
	Vector3::Vector3(double _x, double _y, double _z) {
		v[0] = _x; v[1] = _y; v[2] = _z;
	}
	
	double Vector3::operator[](int _idx) const {
		return v[_idx];
	}
	
	double& Vector3::operator[](int _idx) {
		return v[_idx];
	}
	
	
	
	Vector3 Vector3::operator*(double _a) const {
		const Vector3& v = *this;
		Vector3 res;
		res[0] = _a*v[0];
		res[1] = _a*v[1];
		res[2] = _a*v[2];
		return res;
	}
	
	
	
	Vector3 Vector3::operator-() const {
		const Vector3& v = *this;
		Vector3 res;
		res[0] = -v[0];
		res[1] = -v[1];
		res[2] = -v[2];
		return res;
	}
	
	Vector3 Vector3::operator-(const Vector3& _v2) const {
		return *this + (-_v2);
	}
	
	
	Vector3 Vector3::operator+(const Vector3& _v2) const {
		const Vector3& _v1 = *this;
		Vector3 res;
		res[0] = _v1[0]+_v2[0];
		res[1] = _v1[1]+_v2[1];
		res[2] = _v1[2]+_v2[2];
		return res;
	}
	
	
	Vector3 Vector3::elementMul(const Vector3& _v2) const {
		const Vector3& _v1 = *this;
		Vector3 res;
		res[0] = _v1[0]*_v2[0];
		res[1] = _v1[1]*_v2[1];
		res[2] = _v1[2]*_v2[2];
		return res;
	}
	
	
	Vector3& Vector3::operator=(const Vector3& _v1) {
		Vector3& v = *this;
		v[0] = _v1[0]; v[1] = _v1[1]; v[2] = _v1[2];
		return v;
	}
	
	
	double Vector3::norm() const {
		const Vector3& v = *this;
		return ::sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
	}
	
	double Vector3::max() const {
		const Vector3& v = *this;
		double v12_max = (v[1] > v[2] ? v[1] : v[2]);
		return v[0] > v12_max ? v[0] : v12_max;
	}
	
	double Vector3::sum() const {
		const Vector3& v = *this;
		return v[0] + v[1] + v[2];
	}
	
	double Vector3::dot(const Vector3& _v2) const {
		const Vector3& _v1 = *this;
		return _v1[0]*_v2[0] + _v1[1]*_v2[1] + _v1[2]*_v2[2];
	}
	
	Vector3 Vector3::sqrt() {
		Vector3& v = *this;
		Vector3 res;
		for (int i=0; i<3; ++i) {
			res[i] = ::sqrt(v[i]);
		}
		return res;
	}
	
	
	Vector3 Vector3::rot(const Vector3& _p) const {
		const Vector3& r = *this;
		
		Quaternion q = Quaternion::quaternion(r);
		
		Vector3 res = q.rot(_p);
		
		return res;
	}
	
	
	Vector3 Vector3::cross(const Vector3& _v2) const {
		const Vector3& _v1 = *this;
		Vector3 res;
		res[0] = _v1[1]*_v2[2] - _v1[2]*_v2[1];
		res[1] = _v1[2]*_v2[0] - _v1[0]*_v2[2];
		res[2] = _v1[0]*_v2[1] - _v1[1]*_v2[0];
		return res;
	}
	
	
  std::ostream& operator<<(std::ostream& os, const Vector3& _v) {
	os << "[" << _v[0] << "," << _v[1] << "," << _v[2] << "]";
	return os;
  }
	
  std::ostream& operator<<(std::ostream& os, const Quaternion& _q) {
	os << "[" << _q[0] << "," << _q[1] << "," << _q[2] << "," << _q[3] << "]";
	return os;
  }
	

  std::ostream& operator<<(std::ostream& os, const Matrix3x3& _v) {
	os << "[" << _v(0,0) << "," << _v(0,1) << "," << _v(0,2) << "]" << "[" << _v(1,0) << "," << _v(1,1) << "," << _v(1,2) << "]" << "[" << _v(2,0) << "," << _v(2,1) << "," << _v(2,2) << "]";
	return os;
  }
	   	
	
	Quaternion::Quaternion() {
		q[0] = q[1] = q[2] = q[3] = 0.0f;
	}
	
	Quaternion::Quaternion(double w, double x, double y, double z) {
		q[0] = w; q[1] = x; q[2] = y; q[3] = z;
	}
	
	
	
	Quaternion& Quaternion::operator=(const Quaternion& _q1) {
		Quaternion& q = *this;
		q[0] = _q1[0]; q[1] = _q1[1]; q[2] = _q1[2]; q[3] = _q1[3];
		return q;
	}
	
	
	
	
	double Quaternion::operator[](int _idx) const {
		return q[_idx];
	}
	
	double& Quaternion::operator[](int _idx) {
		return q[_idx];
	}
	
	
	Quaternion Quaternion::operator*(const Quaternion& _q2) const {
		const Quaternion _q1 = *this;
		Quaternion q;
		q[0] = _q1[0]*_q2[0] - _q1[1]*_q2[1] - _q1[2]*_q2[2] - _q1[3]*_q2[3];
		q[1] = _q1[0]*_q2[1] + _q1[1]*_q2[0] + _q1[2]*_q2[3] - _q1[3]*_q2[2];
		q[2] = _q1[0]*_q2[2] - _q1[1]*_q2[3] + _q1[2]*_q2[0] + _q1[3]*_q2[1];
		q[3] = _q1[0]*_q2[3] + _q1[1]*_q2[2] - _q1[2]*_q2[1] + _q1[3]*_q2[0];
		return q;
	}
	
	Quaternion Quaternion::inv() const {
		const Quaternion& q = *this;
		return Quaternion(q[0], -q[1], -q[2], -q[3]);
	}
	
	Vector3 Quaternion::rotvec() const {
		const Quaternion& q = *this;
		Vector3 res;
		double alpha = 2*acos(q[0]);
		double salpha = sin(alpha/2.0f);
		if( salpha == 0 ) {
			res[0] = res[1] = res[2] = 0.0f;
		} else {
			res[0] = alpha*q[1]/salpha;
			res[1] = alpha*q[2]/salpha;
			res[2] = alpha*q[3]/salpha;
		}
		return res;
	}
	
	Quaternion Quaternion::quaternion(const Vector3& _v) {
		Quaternion res;
		double alpha = _v.norm();
		if( alpha == 0 ) {
			res[0] = cos(alpha/2.0f);
			res[1] = res[2] = res[3] = 0;
		} else {
			res[0] = cos(alpha/2.0f);
			res[1] = sin(alpha/2)*_v[0]/alpha;
			res[2] = sin(alpha/2)*_v[1]/alpha;
			res[3] = sin(alpha/2)*_v[2]/alpha;
		}
		return res;
	}
	
	Vector3 Quaternion::rot(const Vector3& _v) const {
		const Quaternion& q = *this;
		Quaternion qv;
		qv[0] = 0; qv[1] = _v[0]; qv[2] = _v[1]; qv[3] = _v[2];
		
		Quaternion resq = q * qv * q.inv();
		Vector3 res;
		res[0] = resq[1]; res[1] = resq[2]; res[2] = resq[3];
		
		return res;
	}
	
	
	
	Matrix3x3::Matrix3x3() {
		
		m[0][0] = 0;
		m[0][1] = 0;
		m[0][2] = 0;
		m[1][0] = 0;
		m[1][1] = 0;
		m[1][2] = 0;
		m[2][0] = 0;
		m[2][1] = 0;
		m[2][2] = 0;
		
	}
	
  
  Matrix3x3::Matrix3x3(double m0, double m1, double m2) {
	
		m[0][0] = m0;
		m[0][1] = 0;
		m[0][2] = 0;
		m[1][0] = 0;
		m[1][1] = m1;
		m[1][2] = 0;
		m[2][0] = 0;
		m[2][1] = 0;
		m[2][2] = m2;
  }

	Matrix3x3::Matrix3x3(double m00, double m01, double m02,
						 double m10, double m11, double m12,
						 double m20, double m21, double m22) {
		
		m[0][0] = m00;
		m[0][1] = m01;
		m[0][2] = m02;
		m[1][0] = m10;
		m[1][1] = m11;
		m[1][2] = m12;
		m[2][0] = m20;
		m[2][1] = m21;
		m[2][2] = m22;
		
	}
	
	
	
	Matrix3x3& Matrix3x3::operator=(const Matrix3x3& _rhs) {
		Matrix3x3& m = *this;
		for (int i=0; i<3; ++i) {
			for (int j=0; j<3; ++j) {
				m.m[i][j] = _rhs.m[i][j];
			}
		}
		return m;
	}
	
	
	Matrix3x3 Matrix3x3::operator+(const Matrix3x3& _rhs) const {
		Matrix3x3 m;
		for (int i=0; i<3; ++i) {
			for (int j=0; j<3; ++j) {
				m.m[i][j] = this->m[i][j] + _rhs.m[i][j];
			}
		}
		return m;
	}
	
	Matrix3x3 Matrix3x3::operator+(const Vector3& _rhs) const {
		Matrix3x3 m;
		for (int i=0; i<3; ++i) {
			for (int j=0; j<3; ++j) {
				m.m[i][j] = this->m[i][j] + (i==j ? _rhs.v[i] : 0);
			}
		}
		return m;
	}
	
	Matrix3x3 Matrix3x3::operator-() const {
		const Matrix3x3& m = *this;
		Matrix3x3 res;
		for( int i=0; i<3; ++i) {
			for( int j=0; j<3; ++j) {
				res.m[i][j] = -m.m[i][j];
			}
		}
		return res;
	}
	
	Matrix3x3 Matrix3x3::operator-(const Matrix3x3& _q_tilde_2) const {
		return *this + (-_q_tilde_2);
	}
	
	
	Matrix3x3 Matrix3x3::operator*(const Matrix3x3& _m2) const {
		const Matrix3x3& _m1 = *this;
		Matrix3x3 res;
		for( int i=0; i<3; ++i) {
			for( int j=0; j<3; ++j) {
				res.m[i][j] = 0.0f;
				for( int k=0; k<3; ++k ) {
					res.m[i][j] += _m1.m[i][k] * _m2.m[k][j];
				}
			}
		}
		return res;
	}
	
	Matrix3x3 Matrix3x3::operator*(double _a) const {
		const Matrix3x3& _m1 = *this;
		Matrix3x3 res;
		for( int i=0; i<3; ++i) {
			for( int j=0; j<3; ++j) {
				res.m[i][j] = _m1.m[i][j] * _a;
			}
		}
		return res;
	}
	
	Vector3 Matrix3x3::operator*(const Vector3& _v) const {
		const Matrix3x3& m = *this;
		Vector3 res;
		for( int i=0; i<3; ++i) {
			res[i] = 0.0f;
			for( int j=0; j<3; ++j) {
				res[i] += m.m[i][j] * _v[j];
			}
		}
		return res;
	}
	
	
	Matrix3x3 Matrix3x3::inv() const {
		const Matrix3x3& m = *this;
		Matrix3x3 res;
		double dinv = 1.0f/this->det();
		res.m[0][0] = dinv * (m.m[2][2]*m.m[1][1]-m.m[2][1]*m.m[1][2]);
		res.m[0][1] = dinv * -(m.m[2][2]*m.m[0][1]-m.m[2][1]*m.m[0][2]);
		res.m[0][2] = dinv * (m.m[1][2]*m.m[0][1]-m.m[1][1]*m.m[0][2]);
		
		res.m[1][0] = dinv * -(m.m[2][2]*m.m[1][0]-m.m[2][0]*m.m[1][2]);
		res.m[1][1] = dinv * (m.m[2][2]*m.m[0][0]-m.m[2][0]*m.m[0][2]);
		res.m[1][2] = dinv * -(m.m[1][2]*m.m[0][0]-m.m[1][0]*m.m[0][2]);
		
		res.m[2][0] = dinv * (m.m[2][1]*m.m[1][0]-m.m[2][0]*m.m[1][1]);
		res.m[2][1] = dinv * -(m.m[2][1]*m.m[0][0]-m.m[2][0]*m.m[0][1]);
		res.m[2][2] = dinv * (m.m[1][1]*m.m[0][0]-m.m[1][0]*m.m[0][1]);
		
		return res;
	}
	
	double Matrix3x3::det() const {
		const Matrix3x3& m = *this;
		return  m.m[0][0]*(m.m[2][2]*m.m[1][1]-m.m[2][1]*m.m[1][2]) -
		m.m[1][0]*(m.m[2][2]*m.m[0][1]-m.m[2][1]*m.m[0][2]) +
		m.m[2][0]*(m.m[1][2]*m.m[0][1]-m.m[1][1]*m.m[0][2]);
	}
	
	
	Matrix3x3 Matrix3x3::identity() {
		Matrix3x3 m;
		for (int i=0; i<3; ++i) {
			for (int j=0; j<3; ++j) {
				m.m[i][j] = (i==j ? 1.f : 0.f);
			}
		}
		return m;
	}
	
	
	Matrix3x3 Matrix3x3::diag(Vector3& _v) {
		Matrix3x3 m;
		for (int i=0; i<3; ++i) {
			for (int j=0; j<3; ++j) {
				m.m[i][j] = i==j ? _v[i] : 0;
			}
		}
		return m;
	}
	
  double Matrix3x3::operator()(int i, int j) const {
	return m[i][j];
  }

	
	
  void coordinateTransform::init() {
	// Initialize with identity transform
	t_hat[0] = t_hat[1] = t_hat[2] = 0.0f;
	r_hat[0] = 1.0;
	r_hat[1] = r_hat[2] = r_hat[3] = 0.0f;
	s_hat = 1.0f;
	
	p_1_0.clear();
	p_2_0.clear();
	bestidx = 0;
	  
  }

	void coordinateTransform::setRefStartPosition(Vector3 _pos) {
		mRefStartPosition = _pos;
	}
	

	coordinateTransform::coordinateTransform() {
		mNeedsInit = true;
	}
	
	
	void coordinateTransform::setOrigin(const Vector3& _v1, const Vector3& _v2) {
		p_1_0.push_back(_v1); p_2_0.push_back(_v2);
	}
	
	
	template <typename T>
	std::vector<size_t> ordered(std::vector<T> const& values) {
		std::vector<size_t> indices(values.size());
		std::iota(begin(indices), end(indices), static_cast<size_t>(0));
		
		std::sort(begin(indices), end(indices),
				  [&](size_t a, size_t b) { return values[a] < values[b]; }
				  );
		return indices;
	}
	
	void coordinateTransform::updateScale(const Vector3& _p_tilde_1, const Vector3& _p_tilde_2) {
		// Measure scale
		double s_tilde, var_hat_s_tilde;
		std::vector<double> scales;
		std::vector<double> var_hat_s_tildes;
		//std::cout << "measured scales: ";
		for( int i=0; i<p_1_0.size(); ++i ) {
			double tscale;
			double tvars;
			measureScale(_p_tilde_1,_p_tilde_2,p_1_0[i],p_2_0[i],
						 sigma_p_1.elementMul(sigma_p_1),
						 (sigma_p_2*0.5).elementMul(sigma_p_2*0.5),mGamma,
						 tscale, tvars);
			scales.push_back(tscale);
			var_hat_s_tildes.push_back(tvars);
			//			std::cout << tvars << " ";
		}
		bestidx = ordered(scales)[scales.size()/2];
		//std::cout << std::endl;

		s_tilde = scales[bestidx];
		var_hat_s_tilde = var_hat_s_tildes[bestidx];
		
		//		std::cout << s_tilde << std::endl;

		//std::sort(scales.begin(), scales.end());

		//s_tilde = scales[scales.size()/2];

		//std::cout << "measurement: " << s_tilde << std::endl;
		//std::cout << "variance:    " << var_hat_s_tilde << std::endl;

		// Predict scale
		double s_hat_minus = s_hat;
		double t = (_p_tilde_2 - p_2_0[bestidx]).dot(_p_tilde_2 - p_2_0[bestidx]) + 0.000001;
		
		Qs = var_hat_s_tilde * sigma_p_2.dot(sigma_p_2)/t;
		
		double Ps_minus = Ps + Qs;
		// Estimate scale

		//std::cout << "Ps_minus: " << Ps_minus << std::endl;

		double K = Ps_minus / (Ps_minus + var_hat_s_tilde);
		
		//std::cout << "K: " << K << std::endl;

		double err = s_tilde - s_hat_minus;


		s_hat = s_hat_minus + K*err;

		Ps = (1-K)*Ps_minus;
		sigma_hat_s_hat = sqrt(Ps);



	}
	
  void coordinateTransform::addBreadcrum( Vector3& _p1, Vector3& _p2 ) {
	
		if ((p_1_0[p_1_0.size()-1]-_p1).norm() > 1.0) {
		  p_1_0.push_back(_p1);
			p_2_0.push_back(_p2);
			std::cout << "added breadcrum (total " << p_1_0.size() << ")\n";
		}
		
		if (p_1_0.size() > 10) {
			p_1_0.erase(p_1_0.begin());
			p_2_0.erase(p_2_0.begin());
		}

  }
	
	void coordinateTransform::updateRotation(const Quaternion& _q_tilde_1, const Quaternion& _q_tilde_2) {
		
		// Get measurement
	  Quaternion r_tilde = _q_tilde_1 * _q_tilde_2.inv();//_q_tilde_2.inv() * _q_tilde_1; // q_2 ptam, q_1 vicon

		// Quaternion::quaternion(r_hat)*_q; // _q ptam----        rh*q2 = q1

		//std::cout << "rotation measurement: " << r_tilde << std::endl;

		Vector3 var_hat_r_tilde = sigma_r_1.elementMul(sigma_r_1) + sigma_r_2.elementMul(sigma_r_2);
		
		// Predict
		Vector3 r_hat_minus = r_hat;
		Matrix3x3 Pr_minus = Pr + Qr;
		
		// Estimate
		Matrix3x3 Kr = Pr_minus * (Pr_minus + var_hat_r_tilde).inv();
		Vector3 err = r_tilde.rotvec() - r_hat_minus;
		r_hat = r_hat_minus + Kr*err;
		Pr = (Matrix3x3::identity() - Kr)*Pr_minus;
		sigma_hat_r_hat = Vector3(Pr.m[0][0],Pr.m[1][1],Pr.m[2][2]).sqrt();
	}
	

	void coordinateTransform::updateOffset(const Vector3& _p_tilde_1, const Vector3& _p_tilde_2,
										   const Quaternion& _q_tilde_1, const Quaternion& _q_tilde_2) {
		
		// Get measurement
		Vector3 t_tilde; Vector3 var_hat_t_tilde;
		measureOffset(_p_tilde_1, _p_tilde_2, s_hat, r_hat, sigma_p_1, sigma_p_2,
					  sigma_hat_s_hat, sigma_hat_r_hat,
					  t_tilde, var_hat_t_tilde);

		// Predict
		Vector3 t_hat_minus = t_hat;
		Vector3 t = _p_tilde_2 - p_2_0[bestidx];

		if( t.norm() < 0.0001 ) {
		  return;
		}

		Qt = Matrix3x3::diag(var_hat_t_tilde) * (sigma_p_2.dot(sigma_p_1) / t.dot(t));
		Matrix3x3 Pt_minus = Pt + Qt;

		
		
		// Estimate
		Matrix3x3 Kt = Pt_minus * (Pt_minus + Matrix3x3::diag(var_hat_t_tilde)).inv();
		Vector3 err = t_tilde - t_hat_minus;
		/*		std::cout << s_hat << "\n" << r_hat << "\n" << sigma_p_1 << " " << sigma_p_2 << "\n" << sigma_hat_s_hat << " " << sigma_hat_r_hat << "\n" << t_tilde << "\n";


		std::cout << "t: " << t << std::endl;
		std::cout << "Pt_minus: " << Pt_minus << std::endl;
		std::cout << "var_hat_t: " << var_hat_t_tilde << std::endl;
		std::cout << "t_hat_minus: " << t_hat_minus << std::endl;
		std::cout << "Kt: " << Kt*err << std::endl;*/
		t_hat = t_hat_minus + Kt*err;
		Pt = (Matrix3x3::identity() - Kt)*Pt_minus;
	}
	
	
	void coordinateTransform::measureScale(const Vector3& _p_tilde_1, const Vector3& _p_tilde_2,
										   const Vector3& _ref1, const Vector3& _ref2,
										   const Vector3& _var1, const Vector3& _var2, double gamma,
										   double& s_tilde, double& var_hat_s_tilde) {
		double d_tilde_1 = (_p_tilde_1 - _ref1).norm();
		double d_tilde_2 = (_p_tilde_2 - _ref2).norm();
		
		double var_d_1 = _var1.max();
		double var_d_2 = _var2.max();
		
		s_tilde = d_tilde_1/(d_tilde_2+0.0001);
		
		double d_hat_1 = d_tilde_1 - gamma*sqrt(var_d_1);
		double d_hat_2 = d_tilde_2 - gamma*sqrt(var_d_2);
		
		double s_hat = s_tilde;
		var_hat_s_tilde = 1e6;
		double error = 1e6;
		
		//		std::cout << "vic diff: " << d_tilde_1 << std::endl;
		//std::cout << "odo diff: " << d_tilde_2 << std::endl;
		if( d_tilde_2 < 6*sqrt(var_d_2) || d_tilde_1 < 6*sqrt(var_d_1)) {
			return;
		}
		

		while( error > 1e-6 ) {
			var_hat_s_tilde = s_hat*s_hat * (var_d_1/(d_hat_1*d_hat_1) + var_d_2/(d_hat_2*d_hat_2));
			double s_sigma = sqrt(var_hat_s_tilde);
			if( gamma*s_sigma > s_tilde ) {
				var_hat_s_tilde = 1e6;
				return;
			}
			error = s_tilde + gamma*s_sigma - s_hat;
			s_hat = s_hat + error;
		}
	}
	

  Vector3 coordinateTransform::refStartPosition() const {
	return mRefStartPosition;
  }

  Vector3 coordinateTransform::transformPosition(const Vector3& _v) const {

	/*	std::cout << "that: " << t_hat;
	std::cout << " rhat: " << r_hat;
	std::cout << " shat: " << s_hat << std::endl;
	*/
	Vector3 res = t_hat + r_hat.rot(_v)*s_hat;

	return res;
  }

  Quaternion coordinateTransform::transformOrientation(const Quaternion& _q) const {
	Quaternion res = Quaternion::quaternion(r_hat)*_q;

	return res;
  }


	
	void coordinateTransform::measureOffset(const Vector3& p_tilde_1, const Vector3& p_tilde_2,
											double s_hat, const Vector3& r_hat,
											const Vector3& sigma_p_1, const Vector3& sigma_p_2,
											double sigma_hat_s, const Vector3& sigma_hat_r,
											Vector3& t_tilde, Vector3& var_hat_t) {
		
		Vector3 p_tilde = r_hat.rot(p_tilde_2) * s_hat;
		
		t_tilde = p_tilde_1 - p_tilde;
		
		Vector3 p_2 = p_tilde_2;
		double s = s_hat;
		Quaternion q = Quaternion::quaternion(r_hat);
		
		Vector3 sigma_hat_p_21_s = q.rot(p_2)*sigma_hat_s;
		
		Vector3 temp[8];
		
		temp[0] = q.rot((Vector3( 1, 1, 1).elementMul(sigma_hat_r)).cross(p_2*s));
		temp[1] = q.rot((Vector3( 1, 1,-1).elementMul(sigma_hat_r)).cross(p_2*s));
		temp[2] = q.rot((Vector3( 1,-1, 1).elementMul(sigma_hat_r)).cross(p_2*s));
		temp[3] = q.rot((Vector3( 1,-1,-1).elementMul(sigma_hat_r)).cross(p_2*s));
		temp[4] = q.rot((Vector3(-1, 1, 1).elementMul(sigma_hat_r)).cross(p_2*s));
		temp[5] = q.rot((Vector3(-1, 1,-1).elementMul(sigma_hat_r)).cross(p_2*s));
		temp[6] = q.rot((Vector3(-1,-1, 1).elementMul(sigma_hat_r)).cross(p_2*s));
		temp[7] = q.rot((Vector3(-1,-1,-1).elementMul(sigma_hat_r)).cross(p_2*s));
		
		Vector3 sum;
		for( int i=0; i<8; ++i ) {
			sum = sum+temp[i].elementMul(temp[i]);
		}
		sum = sum * (1.0/8.0);
		Vector3 sigma_hat_p_21_r = sum.sqrt();
		
		var_hat_t = sigma_p_1.elementMul(sigma_p_1) +
		sigma_p_2.elementMul(sigma_p_2) +
		sigma_hat_p_21_s.elementMul(sigma_hat_p_21_s) +
		sigma_hat_p_21_r.elementMul(sigma_hat_p_21_r);
	}
	
} // namespace sefu

