#include "sensor.h"

#include "coordinate_transform.h"
#include "communication/serial_communication.h"

#include <iostream>
#include <cmath>

#include <boost/thread/lock_guard.hpp>

namespace sefu {
	
	// sefu::comm
	using namespace comm;
	// sefu::proc
	using namespace pars;
	// sefu::filter
	using namespace filt;
	// sefu::cmrc
	using namespace cmrc;
	
	
	std::ostream& operator<<(std::ostream& os, const sOrientation& _ori) {
		os << _ori[0] << "," << _ori[1] << "," << _ori[2] << "," << _ori[3];
		return os;
	}
	std::ostream& operator<<(std::ostream& os, const sPosition& _pos) {
		os << _pos[0] << "," << _pos[1] << "," << _pos[2];
		return os;
	}
	
	
	sensor::sensor() :
	mListener() {
		mType = "";
	}
	
	
	sensor::sensor( listener<pars::processor>* _l, filter* _f, commandReceiver* _cr, const std::string& _type) :
	mType(_type), mListener(_l) , mFilter(_f), mCommandReceiver(_cr) {
		// operator()(boost::shared_ptr<processor>) will be called once new data has arrived
		
		boost::function<void (processor*)> f;
		
		f = std::bind1st(std::mem_fun(&sensor::processNewData),this);
		
		mListener->addCallback(f);
		// Initially doesn't have new
		mHasNew = false;
		// Initially use received quality measure
		mQuality = true;
		// Initially isn't inited
		mInited = false;
	}
	
	
	sensor::sensor(comm::listener<pars::processor>* _l, comm::transmitter* _t, filter* _f, commandReceiver* _cr, const std::string& _type) :
	mType(_type), mListener(_l), mTransmitter(_t) , mFilter(_f), mCommandReceiver(_cr) {
		boost::function<void (pars::processor*)> f;
		
		f = std::bind1st(std::mem_fun(&sensor::processNewData),this);
		
		std::cout << "constructing sensor with transmitter\n";
		
		// operator()(boost::shared_ptr<processor>) will be called once new data has arrived
		mListener->addCallback(f);
		// Initially doesn't have new
		mHasNew = false;
		// Initially use received quality measure
		mQuality = true;
		// Initially isn't inited
		mInited = false;
	}
	
	
	
	// Called asynchronously when new data has arrived
	void sensor::processNewData(pars::processor* _processor) {
		// Get from listener thread, so lock
		boost::lock_guard<boost::mutex> guard(mLock);
		
		// ================= Copy raw sensor data from processor
		// ID of vehicle (same for each sensor)
		mRawSensorData.id = _processor->id();
		// Timestamp (local for sensor)
		mRawSensorData.timestamp = _processor->timestamp();
		// Quality as indicated by the sensor (has track / has scale etc)
		mRawSensorData.quality = _processor->quality();
		// Position and orientation (in quaternions)
		mRawSensorData.position = _processor->position();
		mRawSensorData.orientation = _processor->orientation();
		
		//		if( !mType.compare("ptam") )
		//std::cout << "flag: " << mRawSensorData.quality << std::endl;
		
		// ================= Filter raw sensor data
		if( !mFilter->filterData(mRawSensorData) ) {
			std::cout << "Sensor: " << type() << " jumped. Reinitializing\n";
			if( mTransmitter ) {
				mTransmitter->sendString("r");
			}
			if( mType.compare("vicon") ) {
				mTransform.setNeedsInit();
			}
			mReinitCounter = 0;
		}
		
		//			  std::cout << "vicon roll/pitch: " << thetahalf << " " << phihalf << std::endl;
		// Indicate that we have new data
		mHasNew = true;
		// Now we are initialized
		mInited = true;
	}
	
	
	void sensor::configure(const boost::property_tree::ptree& _pt,
						   const std::string& _thisSensor) {
		mSigmaP = Vector3(_pt.get<float>(_thisSensor + ".sigma_xp"),
						  _pt.get<float>(_thisSensor + ".sigma_yp"),
						  _pt.get<float>(_thisSensor + ".sigma_zp"));
		mSigmaR = Vector3(_pt.get<float>(_thisSensor + ".sigma_xr"),
						  _pt.get<float>(_thisSensor + ".sigma_yr"),
						  _pt.get<float>(_thisSensor + ".sigma_zr"));
		mProcessP = Vector3(_pt.get<float>(_thisSensor + ".process_xp"),
							_pt.get<float>(_thisSensor + ".process_yp"),
							_pt.get<float>(_thisSensor + ".process_zp"));
		
		mPositionOffset = Vector3(_pt.get<float>(_thisSensor + ".offset_x"),
								  _pt.get<float>(_thisSensor + ".offset_y"),
								  _pt.get<float>(_thisSensor + ".offset_z"));
		mRotationOffset = Quaternion::quaternion(Vector3(_pt.get<float>(_thisSensor + ".offset_rx"),
														 _pt.get<float>(_thisSensor + ".offset_ry"),
														 _pt.get<float>(_thisSensor + ".offset_rz")));
		if( !_pt.get<std::string>(_thisSensor + ".role").compare("sensor") ) {
			mRole = SENSOR;
		} else if( !_pt.get<std::string>(_thisSensor + ".role").compare("reference") ) {
			mRole = REFERENCE;
		}
		
		if( !_pt.get<std::string>(_thisSensor + ".coordinate_frame").compare("global") ) {
			mCoordinateFrame = GLOBAL;
		} else if( !_pt.get<std::string>(_thisSensor + ".role").compare("local") ) {
			mCoordinateFrame = LOCAL;
		}
		
		if( !_pt.get<std::string>(_thisSensor + ".fixed_scale").compare("yes") ) {
			mFixedScale = true;
		} else {
			mFixedScale = false;
		}
		
	}
	
	
	void sensor::processCommand(char _c) {
		
		mCommandReceiver->processCommand(_c,this);
		
	}
	
	
	void sensor::initTransform(const sensor_t& _ref) {
		
		boost::lock_guard<boost::mutex> guard(mLock);
		
		mTransform.init();
		mTransform.setRefStartPosition(position());
		setupTransform(_ref);
		if( _ref ) {
		  mTransform.setOrigin(_ref->position(),position());
		} else {
		  mTransform.setOrigin(position(),position());
		}

		
		//std::cout << "Transform position origin: " << position() << "   " << _ref->position() << std::endl;
		//std::cout << "Transform orientation origin: " << orientation() << "   " << _ref->orientation() << std::endl;
		
		std::cout << "Reference origin: " << mTransform.refStartPosition() << std::endl;
		
		mTransform.mNeedsInit = false;
		
	}
	
	void sensor::printTransform() const {
		
		std::cout << std::fixed << std::setprecision(3) << "Scale: " << mTransform.s_hat;
		std::cout << ", offset: " << mTransform.t_hat;
		std::cout << ", rot: " << mTransform.r_hat << std::endl;
		
	}
	
	
	void sensor::updateTransform(const sensor_t& _ref) {
		if( !mType.compare("vicon") ) {
			mTransform.setRefStartPosition(_ref->position());
			return;
		}
		
		if( mTransform.needsInit() ) {
			initTransform(_ref);
			std::cout << "initialized transform\n";
			return;
		}
		
		//std::cout << "vicon raw: " << _ref->transformedOrientation() << std::endl;
		//std::cout << "ptam raw:  " << orientation() << std::endl;
		
		
		
		Quaternion q1raw = _ref->transformedOrientation();
		Vector3 p1raw = _ref->transformedPosition(q1raw);
		
		mRefQuaternion = q1raw;
		
		// Move reference according to sensor location
		Vector3 p1 = p1raw + q1raw.rotvec().rot(mPositionOffset);
		Quaternion q1 = q1raw*mRotationOffset;
		
		Vector3 p11 = p1raw + mPositionOffset;
		
		//std::cout << "vicon to ptam: " << q1 << std::endl;
		
		//		std::cout << "reference for " << mType << ": " << q1 << " : " << p1 << std::endl;
		
		
		if( !mType.compare("odo") && 0) {
			std::cout << "ref type: " << _ref->type() << std::endl;
			std::cout << "ref pos:  " << p1 << std::endl;
			std::cout << "ref ori:  " << q1 << std::endl;
		}
		
		double qv = 180.0/3.1415;
		
		Vector3 p2 = position();
		Quaternion q2 = orientation();
		
		if( !mType.compare("ptam") ) {
			/*std::cout << "raw ptam data " << mType << ": " << p2 << std::endl;
		  std::cout << "position offset " << mPositionOffset << std::endl;
		  std::cout << "raw vicon data " << p1raw << std::endl;
		  std::cout << "transformed vicon data " << p1 << std;
		  std::cout << "raw ptam ori " << q2 << std::endl;
		  std::cout << "transformed vicon ori " << q1 << std::endl;*/
		}
		
		
		
		mTransform.updateRotation(q1, q2);
		
		if(mTransform.p_1_0[mTransform.bestidx][0] != p1[0] && mTransform.p_2_0[mTransform.bestidx][0] != p2[0]) {
			mTransform.updateScale(p1, p2);
			
			mTransform.updateOffset(p1, p2, q1, q2);
			
			mFilter->setScale(mTransform.s_hat);
			
			mTransform.addBreadcrum(p1,p2);
			
			//			if( !mType.compare("odo") )
			// std::cout << mType << " transform: " << mTransform.t_hat << std::endl;
		}
		
		//std::cout << "ptam transform: " << Quaternion::quaternion(mTransform.r_hat) << std::endl;
		
	}
	
	
	void sensor::resetTransform() {
		mTransform.reset();
	}
	
	double sensor::confidence() const {
		if(!mType.compare("vicon")) {
			return 1.0;
		} else if (!mType.compare("ptam")) {
			return 0.5;
		} else if (!mType.compare("odo")) {
			return 0.25;
		}

		return 0.0;
	}
	
	
	void sensor::toggleQualityOverride() {
		mQuality = !mQuality;
	}
	
	void sensor::setupTransform(const sensor_t& _ref) {
		
		// Measurement noise
	  mTransform.sigma_p_1 = (!_ref ? sefu::Vector3(0,0,0) : _ref->sigmaP());
		mTransform.sigma_p_2 = mSigmaP;
		
		mTransform.sigma_r_1 = (!_ref ? sefu::Vector3(0,0,0)  : _ref->sigmaR());
		mTransform.sigma_r_2 = mSigmaR;
		
		mTransform.mGamma = 2;
		
		// Process noise
		mTransform.Qs = 0;
		mTransform.Qr = Matrix3x3(mProcessP[0],mProcessP[1],mProcessP[2]);
		mTransform.Qt = Matrix3x3();
		
		// Initial estimates
		mTransform.s_hat = 1.0;
		mTransform.r_hat = Vector3(0,0,0);
		mTransform.t_hat = Vector3(0,0,0);
		
		// Initial estimates of covariances
		// (If this sensor is the reference sensor, use no process noise);
		if( !_ref || &(*_ref) != this ) {
			mTransform.Ps = 1e6;
			mTransform.Pr = Matrix3x3::identity()*1e6;
			mTransform.Pt = Matrix3x3::identity()*1e6;
		} else {
			mTransform.Ps = 0;
			mTransform.Pr = mTransform.Pt = Matrix3x3::identity()*0.0;
		}
		
	}
	
	std::string sensor::logMessage(bool b) {
		std::stringstream ss;
		
		int id = 0;
		if( !mType.compare("ptam") ) id = 1;
		if( !mType.compare("odo") ) id = 2;
		
		ss << id << ",";
		if(!b)
			ss << mRawSensorData.quality;
		else
			ss << 1;
		
		const Quaternion& q = transformedOrientation();
		float heading = atan2(2*(q[0]*q[3] + q[1]*q[2]), 1-2*(q[2]*q[2] + q[3]*q[3]));
		
		//		std::cout << mType<< ": q " << q << ", heading: " << heading << std::endl;
		
		ss	<< "," //<< std::fixed << std::setprecision(3)
		<< transformedPosition(q)[0] << "," << transformedPosition(q)[1] << ","
		<< heading << "," << 0.0 << "," << 0.0 << "," << 0.0;
		
		return ss.str();
	}
	
	
	std::string sensor::logMessageRaw(bool b) {
		std::stringstream ss;
		
		int id = 0;
		if( !mType.compare("ptam") ) id = 1;
		if( !mType.compare("odo") ) id = 2;
		
		ss << id << ",";
		if(!b)
			ss << mRawSensorData.quality;
		else
			ss << 1;
		
		const Quaternion& q = orientation();
		double heading = atan2(2*(q[0]*q[3] + q[1]*q[2]), 1-2*(q[2]*q[2] + q[3]*q[3]));
		
		ss	<< "," << std::fixed << std::setprecision(3)
		<< position()[0] << "," << position()[1] << ","
		<< heading << "," << 0.0 << "," << 0.0 << "," << 0.0;
		
		return ss.str();
	}
	
	void sensor::sendMessage(char _message) {
		if(mTransmitter) {
			mTransmitter->sendRaw(_message);
		}
	}
	
	
	bool sensor::hasTracking() {
		return mRawSensorData.quality >= 1;
	}
	
	
	bool sensor::hasScale() {
	  bool hasFixedScale =  mFixedScale || mRawSensorData.quality >= 2;
	  // If sensor has a fixed scale, or the sensor measured the scale, we have scale
	  if( hasFixedScale ) {
	    return true;
	  } else {
	    // Else if we are far enough from the starting position we also should have
	    // a good estimated scale
	    if( (mTransform.refStartPosition() - position()).norm() > 2.5 ) {
	      return true;
	    }
	  }
	  // Otherwise we don't
	  return false;
	}
	
	
	bool sensor::valid() const {
		return mType.compare("");
	}
	
	
	void sensor::start() {
		mListener->start();
	}
	
	
	void sensor::stop() {
		mListener->stop();
	}
	
	
	const std::string& sensor::type() const {
		return mType;
	}
	
	
	short sensor::quality(const sensor_t _reference) const {
		bool quality = mRawSensorData.quality && mQuality;
		if( _reference ) {
			quality &= this->compareRollAndPitch(_reference);
		}
		return quality;
	}
	
	bool sensor::compareRollAndPitch(const boost::shared_ptr<sensor> _reference) const {
		return true;
	}
	
	size_t sensor::timestamp() const {
		return mRawSensorData.timestamp;
	}
	
	
	int sensor::getId() const {
		return mRawSensorData.id;
	}
	
	
	void sensor::fillPoseUntransformed(float position[3], float orientation[4]) {
		// Might be set by listener thread, so lock
		boost::lock_guard<boost::mutex> guard(mLock);
		
		position[0] = this->position()[0];
		position[1] = this->position()[1];
		position[2] = this->position()[2];
		
		orientation[0] = this->orientation()[0];
		orientation[1] = this->orientation()[1];
		orientation[2] = this->orientation()[2];
		orientation[3] = this->orientation()[3];
	}
	
	
	void sensor::fillPose(float position[3], float orientation[4]) {
		// Might be set by listener thread, so lock
		boost::lock_guard<boost::mutex> guard(mLock);
		
		orientation[0] = transformedOrientation()[0];
		orientation[1] = transformedOrientation()[1];
		orientation[2] = transformedOrientation()[2];
		orientation[3] = transformedOrientation()[3];
		
		Quaternion q(orientation[0], orientation[1],orientation[2],orientation[3]);
		
		position[0] = transformedPosition(q)[0];
		position[1] = transformedPosition(q)[1];
		position[2] = transformedPosition(q)[2];
		
	}
	
	
	bool sensor::canUse(const boost::shared_ptr<sensor>& _ref)  {
		
		//	std::cout << "checking if can use " << type() << std::endl;
		//if( !mType.compare("ptam") )
		//std::cout << mTransform.refStartPosition() << "    " << position() << std::endl;
		
		if( !hasScale() ) {
		  return false;
		}
		
		if( !mType.compare("vicon") ) {
		  return true;
			if( mReinitCounter >= 3 ) {
				mReinitCounter = 0;
				return true;
			} else {
				mReinitCounter++;
				return false;
			}
		}
		
		
		if( mTransform.mNeedsInit ) {
		  //std::cout << "needs init" << std::endl;
			this->initTransform(_ref);
			return false;
		} else {
			mReinitCounter++;
			if( mReinitCounter >= 3 ) {
				return true;
			} else {
			  //std::cout << ": counting, but not yet\n";
			}
		}
		
		return false;
	}
	
	Vector3 sensor::position() const {
		return Vector3(mRawSensorData.position[0],mRawSensorData.position[1],mRawSensorData.position[2]);
	}
	
	Quaternion sensor::orientation() const {
		return Quaternion(mRawSensorData.orientation[0],mRawSensorData.orientation[1],mRawSensorData.orientation[2],mRawSensorData.orientation[3]);
	}
	
	Vector3 sensor::transformedPosition(const Quaternion& _refq) const {
		Vector3 transformed = mTransform.transformPosition(Vector3(mRawSensorData.position[0],mRawSensorData.position[1],mRawSensorData.position[2]));
		
		// Move back to the global vehicle reference
		
		/*		if(!mType.compare("ptam") ) {
		 std::cout << "send transformed: " << transformed << std::endl;
		 }*/
		
		return  transformed - _refq.rotvec().rot(mPositionOffset);//mRotationOffset.inv().rotvec().rot(transformed) - mPositionOffset;
		
	}
	
	Quaternion sensor::transformedOrientation() const {
		Quaternion transformed = mTransform.transformOrientation(Quaternion(mRawSensorData.orientation[0],mRawSensorData.orientation[1],mRawSensorData.orientation[2],mRawSensorData.orientation[3]));
		
		// Rotate back to the global vehicle reference q1raw*mRotationOffset;
		
		//		std::cout << mTransform.t_hat << " " << mTransform.s_hat << " " << mTransform.r_hat << std::endl;
		
		//		if( !mType.compare("ptam") )
		//std::cout << "Ptam raw quaternion: " << Quaternion(mRawSensorData.orientation[0],mRawSensorData.orientation[1],mRawSensorData.orientation[2],mRawSensorData.orientation[3]) << std::endl;
		
		Quaternion res = transformed*mRotationOffset.inv();
		
		//std::cout << "transformed back " << mType << ": " << res << std::endl;
		
		return res;
		
	}
	
} // namespace sefu
