#include "command_receivers.h"

#include "sensor.h"

namespace sefu { namespace cmrc {
	
	void ptamCommandReceiver::processCommand(char _c, sensor* _s) {
		std::cout << "command received: " << _c << std::endl;
		switch(_c) {
			case 'r':
			  _s->sendMessage('r');
			  break;
				
			default:
				break;
		}
	}
	
} // namespace cmrc
} // namespace sefu
